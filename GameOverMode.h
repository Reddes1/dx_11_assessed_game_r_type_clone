#pragma once

#include "Game.h"            //Game Manager
#include "ModeInterface.h"   //Mode Parent
#include "Sprite.h"          //For making sprite vector arrays
#include "SpriteFont.h"      //For screen text


/*
    Basic Game Over state that bridges the active mode to a highscore state.
*/
class GameOverMode : public iMode
{
public:
    //HMode Handle
    inline static const std::string MODE_NAME = "GAME_OVER";

    GameOverMode();
    ~GameOverMode();

    //Parent overrides
    void Enter() override;
    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ProcessKey(char key) override;
    std::string GetModeName() const override { return MODE_NAME; }


private:
    std::vector<Sprite> backgrounds_;

    //Fonts
    DirectX::SpriteFont* font1_;    //Algerian

    //timer container
    float gameOverElapsed_ = 0;

    //Initialise block
    void InitBackground();
    //Render any screen text
    void RenderText(DirectX::SpriteBatch& batch);
    //Release resources
    void Release();
};