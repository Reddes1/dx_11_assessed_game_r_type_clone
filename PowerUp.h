#pragma once

#include "EntityInterface.h"    //Object Parent
#include "Sprite.h"             //Sprite class
#include "StageOneMode.h"       //Mode Owner
#include "FlagSets.h"           //Object state flags

enum class STATES
{
    CONTAINER, POWER_UP
};
enum class POWER_UP_TYPE
{
    SPEED, WEAPON, BOMB
};
/*
    PowerUp class that serves as a container for a PowerUp for the player, wwitching to the pick-up
    when destroyed.
*/
class PowerUp : public iEntity
{
public:

    PowerUp();
    ~PowerUp()
    {}

    //Parent overrides
    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ResetVariables() override;

    //Set parent mode
    void SetMode(StageOneMode& mode) { ownerMode_ = &mode; }

    POWER_UP_TYPE GetPowerUpType() { return powerUpType; }
    STATES GetCurrentState() { return state; }

private:
    //What mode does this sprite belong to
    StageOneMode* ownerMode_ = nullptr;

    //Flags
    ObjectStateFlags flags1_;
    bool isNowOnScreen = false;
    //States
    STATES state;
    POWER_UP_TYPE powerUpType;

    //Initialise
    void Init() override;
    //Update logic for when the object is in container mode
    void UpdateContainer(float dTime);
    //Update logic for when the object is now a powerup
    void UpdatePowerUp(float dTime);
    //Validate the position of the object when container, and change velocity on play area collision
    void ValidatePosition(Vector2& pos);
    //Manage the state switch
    void ProcessStateSwitch();

};