#include "Enemy1.h"
#include "GameConstants.h"  //Frame data and value constants

Enemy1::Enemy1()
    : iEntity(Game::Get().GetD3D())
{
    Init();
}

void Enemy1::Init()
{
    //Handy Handles
    MyD3D& d3d = Game::Get().GetD3D();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Calulate and capture adjustments for screen size
    adj_.scaleAdj = Vector2(width / FC::DEFAULT_RES.x, height / FC::DEFAULT_RES.y);
    adj_.spdAdj = Vector2(width / FC::DEFAULT_RES.x, height / FC::DEFAULT_RES.y);

    //Load texture and set frames
    std::vector<RECTF> frames(FD::stageOneEnemyAtlasFrames,
        FD::stageOneEnemyAtlasFrames + sizeof(FD::stageOneEnemyAtlasFrames) / sizeof(FD::stageOneEnemyAtlasFrames[0]));
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "data/sprites/stageone/stageoneatlas.dds",
        "Stage One Atlas 1", false, &frames);

    //Set object parameters
    spr_.SetTexture(*p);
    spr_.SetOrigin(Vector2((frames[0].right - frames[0].left) * 0.5f, (frames[0].bottom - frames[0].top) * 0.5f));
    spr_.SetScale(Vector2(adj_.scaleAdj.x * GC::E1_SCALE, adj_.scaleAdj.y * GC::E1_SCALE));
    spr_.GetAnimation().Initialise(0, 7, 8, true);
    spr_.GetAnimation().Play(true);
    health_ = GC::E1_STARTING_HP;
}

void Enemy1::Update(float dTime)
{
    //If HP = 0, and flagged that the object just died.
    if (health_ <= 0 && !flags1_.isDead && isActive_) 
    {
        flags1_.isDead = true;
    }

    //Check if object just died, else if check active state
    if (flags1_.isDead)
    {
        isActive_ = false;
        flags1_.isDead = false;
        Game::Get().GetScoreManager().UpdateActivePlayerScore(8);
    }
    else if (isActive_)
    {
        Vector2 pos = spr_.GetPosition();
        //Calculate new position
        pos += spr_.GetVelocity() * (GC::E1_SPEED * adj_.spdAdj) * dTime;
        //Set new posiiton
        spr_.SetPosition(pos);

        //Become inactive when offscreen
        if (spr_.GetPosition().x < 0 - spr_.GetScreenSize().x)
        {
            isActive_ = false;
        }
        spr_.GetAnimation().Update(dTime);
    }
}

void Enemy1::Render(float dTime, DirectX::SpriteBatch& batch)
{
    //No special rendering, let the parent handle it
    iEntity::Render(dTime, batch);
}

void Enemy1::ResetVariables()
{
    health_ = GC::E1_STARTING_HP;
    spr_.SetVelocity(Vector2(-1, 0));
}

