#pragma once

/*
    GC = Game Constants. Anything specifically relating to gameplay orentatied variables like speeds, sizes, durations,
    hp etc. 
*/
namespace GC
{
    //////////////////////
    ///Player Constants///
    //////////////////////

    //Player Control Method Speeds
    const float P_KB_SPEED = 350;
    const float P_PAD_SPEED = 350;
    const float P_SHOT_DELAY = 0.42f;

    //Player Projectiles
    const float P_PROJ1_SPEED = 500.f;
    const float P_PROJ1_SCALE = 0.6f;
    const int P_PROJ1_DMG = 1;

    const float P_PROJ2_SPEED = 480.f;
    const float P_PROJ2_SCALE = 3.f;
    const int P_PROJ2_DMG = 2;

    const float P_PROJ3_SPEED = 300.f;
    const float P_PROJ3_SCALE = 1.f;
    const int P_PROJ3_DMG = 5;
    
    //Player Sprite(s)
    const float P_SHIP_SCALE = 1.85f;
    const float P_THRUSTER_DURATION = 0.2f;

    //Player Weapons/Speeds
    const int P_MAX_WEAPON_LEVEL = 1;
    const int P_MAX_SPEED_LEVEL = 2;
    const float P_SPEED_LV_1 = 50.f;
    const float P_SPEED_LV_2 = 100.f;
    

    //////////////////////
    ///Enemy1 Constants///
    //////////////////////

    const float E1_SCALE = 2.f;
    const int E1_STARTING_HP = 2;
    const float E1_SPEED = 190.f;


    //////////////////////
    ///Enemy2 Constants///
    //////////////////////

    const float E2_SCALE = 2.2f;
    const float E2_PROJ_SCALE = 2.0f;
    const int E2_STARTING_HP = 3;
    const float E2_SPEED = 140.f;
    const float E2_PROJ_SPEED = 180.f;
    const float E2_SHOT_DELAY = 1.f;


    ////////////////////////
    ///Power-Up Constants///
    ////////////////////////

    const float PU_SCALE = 2.0f;
    const int PU_STARTING_HP = 3;
    const float PU_SPEED = 150.f;


    //////////////////////
    ///Stage One Consts///
    //////////////////////
    
    //Background
    const float BGND_SCROLL_SPD = 10.f;

    //Stage Timers
    const float STAGE1_TIME_GATE1 = 30.f;
    const float STAGE1_TIME_GATE2 = 60.f;
    const float STAGE1_TIME_GATE3 = 90.f;
    const float STAGE1_TIME_GATE4 = 120.f;
    const float STAGE1_TIME_GATE5 = 150.f;
    const float STAGE1_TIME_GATE6 = 180.f;

    //Spawn Delay Timers
    const float PU_SPAWN_DELAY = 20.f;
    const float E1_SPAWN_DELAY1 = 5.f;
    const float E1_SPAWN_DELAY2 = 4.f;
    const float E2_SPAWN_DELAY1 = 6.f;
    const float E2_SPAWN_DELAY2 = 5.f;
    const float E2_SPAWN_DELAY3 = 4.f;
    const float E2_SPAWN_DELAY4 = 0.7f;
}


/*
    FC = File Constants. Things like layer counts, sprite counts, sprite res's. Things that relate to the files or
    the number of things we want when initialising the program.
*/
namespace FC
{
    //Default resolution we want to scale most if not all the sprites to.
    const Vector2 DEFAULT_RES = { 1920, 1080 };

    //////////////////////
    ///Player Constants///
    //////////////////////

    const int P_ANIM_SPEED = 5;

    ///////////////////////
    ///Splash Mode Const///
    ///////////////////////

    const int SPLASH_BG_COUNT = 1;


    ///////////////////////
    ///Intro  Mode Const///
    ///////////////////////

    const int INTRO_BG_COUNT = 1;


    //////////////////////////
    ///Main Menu Mode Const///
    //////////////////////////

    const int MAIN_MENU_BG_COUNT = 1;
    const int MENU_BUTTON_COUNT = 3;


    //////////////////////////
    ///HowToPlay Mode Const///
    //////////////////////////

    const int HTP_BG_COUNT = 2;


    //////////////////////
    ///Stage One Consts///
    //////////////////////

    const int STAGE_ONE_BG_COUNT = 9;
    const int P_PROJ1_COUNT = 48;
    const int P_PROJ2_COUNT = 48;
    const int E2_PROJ_COUNT = 48;
    const int PROJ_RESERVE = 1 + P_PROJ1_COUNT + P_PROJ2_COUNT + E2_PROJ_COUNT;

    const int E1_COUNT = 64;
    const int E2_COUNT = 32;
    const int E3_COUNT = 32;
    const int PU_COUNT = 8;
    const int OBJECT_RESERVE = 1 + E1_COUNT + E2_COUNT + E3_COUNT + PU_COUNT;

    const int DEFAULT_BG_SEED = 1;

    //////////////////////////
    ///Game Over Mode Const///
    //////////////////////////

    const int GAME_OVER_BG_COUNT = 1;

    //////////////////////////
    ///Highscore Mode Const///
    //////////////////////////

    const int HIGH_SCORE_BG_COUNT = 1;

}

/*
    MC = Mode Constants. Mode specific variables like timers for transitions, duration of something being on screen for
    the mode (slideshow maybe?).
*/
namespace MC
{

    ///////////////////////
    ///Splash Mode Const///
    ///////////////////////

    const float SPLASH_1_DURATION = 3;

    ///////////////////////
    ///Intro  Mode Const///
    ///////////////////////


    //////////////////////////
    ///Main Menu Mode Const///
    //////////////////////////


    //////////////////////
    ///Stage One Consts///
    //////////////////////

    //////////////////////
    ///Game Over Consts///
    //////////////////////

    const float GAME_OVER_DURATION = 5;


    ///////////////////////
    ///Highscores Consts///
    ///////////////////////

    const int NAME_MAX_SIZE = 5;
}

/*
    FD = Frame Data. Commonly shared frame data that use the same atlas stored in the same location. 
*/
namespace FD
{
    const RECTF backgroundAtlasFrames[]
    {
        {0, 0, 1919, 1079},         //Intro Screen
        {0, 1100, 1919, 2178},      //Main Menu Screen
        {0, 2199, 1919, 3279},      //HighScore Screen
        {1950, 0, 3869, 1079},      //Gameover Screen
        {1950, 1100, 3869, 2178},   //Pause Screen
        {1950, 2199, 3869, 3279}    //HowToPlay Overlay
    };

    const RECTF stageOneBackgroundAtlasFrames[]
    {
        {0, 0, 1920, 1080},         //Frame 0 (Star background
        {2, 1154, 252, 1406},       //Blue Planet 1
        {257, 1154, 507, 1406},     //Blue Planet 2
        {512, 1154, 762, 1406},     //Red Planet 1
        {760, 1154, 1008, 1406},    //Red Planet 2
        {1013, 1154, 1263, 1406},   //Pink Planet 1
        {1268, 1154, 1509, 1406},   //Green Planet 1
        {1514, 1154, 1753, 1406},   //Blue Planet 3
        {1758, 1154, 2001, 1406}    //Green Planet 2
    };

    const RECTF stageOneEnemyAtlasFrames[]
    {
        {0, 409, 24, 433},          //Enemy 1 Start (Frames 0-7)
        {32, 409, 56, 433},
        {65, 409, 88, 433},
        {96, 409, 120, 433},
        {128, 409, 152, 433},
        {160, 409, 184, 433},
        {192, 409, 216, 433},
        {224, 409, 248, 433},       //Enemy 1 End
        {0, 435, 29, 461},          //Enemy 2 Start (Frames 8-13)
        {44, 435, 73, 461},
        {88, 435, 117, 461},
        {132, 435, 161, 461},
        {176, 435, 205, 461},
        {216, 435, 245, 461},
        {2, 2, 101, 401},           //Wall 1 Start (Frames 14-17)
        {111, 2, 210, 401},
        {222, 2, 321, 401},
        {333, 2, 432, 401},         //Wall 1 End
        {5, 465, 27, 492},          //Power Up Container (Frame 18)
        {33, 464, 56, 492},         //Speed Powerup (Frame 19)
        {59, 464, 81, 492},         //Shot Powerup (Frame 20)
        {85, 464, 107, 488}         //Bomb Powerup (Frame 21)
    };

    const RECTF projectileAtlas1Frames[]
    {
        {1, 16, 10, 21},            //Regular Shot
        {14, 15, 29, 22},           //Powered-Up Shot
        {50, 1, 85, 32},            //Charged Shot/Bomb
        {3, 37, 9, 44}              //Enemy Shot
    };
}