#include "ScoreManager.h"
#include <algorithm>            //std::sort
#include <assert.h>
#include <iostream>
#include <fstream>

ScoreManager::ScoreManager()
{
    scoresTable_.reserve(16);
    LoadFileDataIntoTable();
}

ScoreManager::~ScoreManager()
{
    Release();
}

void ScoreManager::LoadFileDataIntoTable()
{
    //Open filestream
    std::fstream myFile;
    myFile.open("files/highscores.txt", std::fstream::in);
    
    //If the file didn't open, make a new text file
    if (!myFile.is_open())
    {
        //Create new data
        CreateNewTableData();
        
        //Reopen
        myFile.open("files/highscores.txt", std::fstream::in);
    }

    //Temporary containers
    std::vector<std::string> strings;
    std::string line;
    ScoreContainer newEntry;
    
    //Push string lines into to temporary vector
    while (std::getline(myFile, line, ','))
    {  
        strings.push_back(line);
    }

    //Take the seperate strings and put them into their appropidate containers/variables
    for (int i(0); i < strings.size() * 0.5f; ++i)
    {
        newEntry.name = strings[i * 2];
        newEntry.score = std::stoi(strings[(i * 2) + 1]);
        scoresTable_.push_back(newEntry);
    }

    //Close file stream
    myFile.close();
    assert(!myFile.is_open());
}

void ScoreManager::SortHighScoreTable()
{
    //Sort score table, putting highest first.
    std::sort(scoresTable_.begin(), scoresTable_.end(), [](const auto& lSide, const auto& rSide)
        {
            return static_cast<float>(lSide.score) > static_cast<float>(rSide.score);
        }
    );
}

void ScoreManager::CreateNewTableData()
{
    //Create new file
    std::fstream myFile;
    myFile.open("files/highscores.txt", std::ios::out);
    assert(myFile.is_open());

    const int SIZE = 10;

    //New data
    const char* names[SIZE] = { "Rick", "Morty", "Rach", "Jacky", "Boiii", "Easyy", "Jon", "snow", "Krick", "Jones" };
    const int scores[SIZE] = { 500, 450, 400, 350, 300, 250, 200, 150, 100, 50 };

    //Add the data to file
    for (int i(0); i < SIZE; ++i)
    {
        myFile << names[i] << "," << scores[i] << ",";
    }  

    //Close stream
    myFile.close();
    assert(!myFile.is_open());

}

void ScoreManager::AddActivePlayerToTable()
{
    //Push data in
    scoresTable_.push_back(currentPlayerData_);
    //Sort the data
    SortHighScoreTable();
}

void ScoreManager::SaveDataToFile()
{
    //Open file
    std::fstream myFile;
    myFile.open("files/highscores.txt", std::ios::out | std::ios::trunc);
    assert(myFile.is_open());

    //Write new data to file
    for (size_t i(0); i < scoresTable_.size(); ++i)
    {
        myFile << scoresTable_[i].name << "," << std::to_string(scoresTable_[i].score) << ",";
    }

    //Close file stream
    myFile.close();
}

void ScoreManager::Release()
{
    scoresTable_.clear();
}
