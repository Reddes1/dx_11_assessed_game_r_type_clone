#pragma once
#include "Sprite.h"
#include "Game.h"

//Store the values needed to adjust pixel sensitive values abstractly for use in calculations.
struct ResAdjustValues
{
    //Capture the speed adjust when calulating speed in pixels and needing to account for other resolutions
    Vector2 spdAdj{ 1, 1 };
    //Capture the scale adjust when sizing an object for different resolutions
    Vector2 scaleAdj{ 1, 1 };
    //Expand Here
};

/*
Parent for any game object. Provides multiple interfaces for health, damage and activity as well
as a default sprite. Initialise derived classes with D3D reference.
*/
class iEntity
{
public:
    iEntity(MyD3D& d3d)
        : spr_(d3d), health_(1), damageValue_(1), isActive_(false)
    {}
    virtual ~iEntity() {}

    //Initialise object with textures, variables etc
    virtual void Init() = 0;
    //Update object variables
    virtual void Update(float dTime) = 0;
    //Draw the sprite
    virtual void Render(float dTime, DirectX::SpriteBatch& batch) { spr_.Draw(batch); }

    //Set if the object is active or not
    void SetIsActive(bool state) { isActive_ = state; }
    //Return the active state of the object
    bool const GetIsActive() const { return isActive_; }

    /*
    Game objects are likely to have hp values, so here are some interfaces and a basic hp container
    to represent that.
    */

    //Increase hp value
    virtual void IncreaseHPValue(float value) { health_ += value; }
    //Decrease hp value
    virtual void ReduceHPValue(float value) { health_ -= value; }
    //Set the hp to specific value
    virtual void SetHPValue(float newValue) { health_ = newValue; }
    //Return current hp of object
    virtual const float GetHPValue() { return health_; }

    /*
        Game objects may also do damage to other objects so here are some interfaces and a singular damage
        value for simple objects (complex damage value structures should be handled by the child, interfaces available
        for complex usage).
    */

    //Get single damage value
    virtual float GetDamageValue() { return damageValue_; }
    //Set single damage values
    virtual void SetDamageValue(float newValue) { damageValue_ = newValue; }
    //Get the damage value at the index
    virtual float GetDamageValueAtIndex(int index) { return NULL; }
    //Set the damage value at the index
    virtual void SetDamageValueAtIndex(int index, float value) { }

    //Refresh/Reset certain values of the object (like hp)
    virtual void ResetVariables() { }
        
    //Game like collision, so returns a rect using the objects current position and screensize.
    Vector4 GetCollisionValues()
    {
        Vector2 pos = spr_.GetPosition();
        Vector2 dim = spr_.GetScreenSize();
        Vector4 rect = Vector4(pos.x, pos.y, dim.x, dim.y);
        return rect;
    }

    //Get a reference of the sprite object
    Sprite& GetSprite() { return spr_; }

protected:

    //Sprite adjustments
    ResAdjustValues adj_;
    //Default sprite object
    Sprite spr_;
    //Game objects usually have health
    float health_;
    //Basic damage value for use
    float damageValue_;
    //if active, render, update etc
    bool isActive_;
};
