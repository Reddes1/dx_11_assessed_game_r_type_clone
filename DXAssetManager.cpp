#include "DXAssetManager.h"


void DXAssetManager::Release()
{
    //Uses the type pair(defines first/second) to clear the mapped value (the second)
    for (auto& pair : texCache_)
        ReleaseCOM(pair.second.pTex);

    //Clear the rest of the cached data
    texCache_.clear();
}

//C++ 17 Required for filesystem
ID3D11ShaderResourceView* DXAssetManager::LoadTexture(ID3D11Device* pDevice, const std::string& fileName,
    const std::string& texNickName, bool appendPath, const std::vector<RECTF>* frames)
{
    //If no nickname or texture name given, find texture through filepath and give new name
    std::string name = texNickName;
    if (name.empty())
    {
        std::filesystem::path p(fileName);
        name = p.stem().string();
    }

    //Search cache for texture
    TexMap::iterator iter = texCache_.find(name);
    //If the iterator found the texture being looked for
    if (iter != texCache_.end())
        return (*iter).second.pTex;

    //For path manipulation
    const std::string* tempPath = &fileName;
    std::string path;
    //If appending a file path struture 
    if (appendPath)
    {
        path = assetAppendPath_ + fileName;
        tempPath = &path;
    }
    //Set load path from manipulated path
    std::wstring compFilePath(tempPath->begin(), tempPath->end());

    //Load Texture
    DDS_ALPHA_MODE alpha;
    ID3D11ShaderResourceView* pT = nullptr;

    //If load fails
    if (CreateDDSTextureFromFile(pDevice, compFilePath.c_str(), nullptr, &pT, 0, &alpha) != S_OK)
    {
        WDBOUT(L"Cannot load " << tempPath << L"\n");
        assert(false);
        return false;
    }

    //Save into cache
    assert(pT);
    texCache_.insert(TexMap::value_type(name, Data(fileName, pT, GetDimensions(pT), frames)));


    return pT;
}

//Return Vec2 Dimensions
Vector2 DXAssetManager::GetDimensions(ID3D11ShaderResourceView* pTex)
{
    assert(pTex);
    //Get the texture resource
    ID3D11Resource* res = nullptr;
    pTex->GetResource(&res);
    assert(res);

    //Query
    ID3D11Texture2D* tex2D = nullptr;
    HRESULT hr = res->QueryInterface(&tex2D);

    //Return vector
    Vector2 dim(0, 0);

    //If query worked
    if (SUCCEEDED(hr))
    {
        //Get a description of the texture file and set values from it
        D3D11_TEXTURE2D_DESC desc;
        tex2D->GetDesc(&desc);
        dim = { static_cast<float>(desc.Width), static_cast<float>(desc.Height) };
    }

    //Release resources
    ReleaseCOM(tex2D);
    ReleaseCOM(res);

    return dim;
}

//Get texture slowly
const DXAssetManager::Data& DXAssetManager::GetHandle(ID3D11ShaderResourceView* pTex)
{
    //Iterate through cache
    TexMap::iterator iter = texCache_.begin();

    //Return pointer
    Data* p = nullptr;

    //If not at end and pointer not null
    while (iter != texCache_.end() && !p)
    {
        //if iterator finds texture
        if ((*iter).second.pTex == pTex)
            p = &(*iter).second;
        ++iter;
    }
    assert(p);

    return *p;
}