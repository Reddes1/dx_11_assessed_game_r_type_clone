#pragma once
#include "EntityInterface.h"    //Object Parent
#include "Sprite.h"             //Sprite class
#include "StageOneMode.h"       //Mode Owner
#include "FlagSets.h"           //bool flag objects

//Animation frames
const RECTF thrustAnimFrames[]{
     { 0,  0, 15, 16},
     { 16, 0, 31, 16 },
     { 32, 0, 47, 16 },
     { 48, 0, 64, 16 }
};
const RECTF shipAnimFrames[]
{
     { 0, 17, 25, 31},     //Frame Up 2
     { 26, 0, 51, 15},     //Frame Up 1
     { 0, 0, 25, 15},      //Frame Neutral
     { 26, 17, 51, 31},    //Frame Down 1
     { 0, 34, 25, 48}      //Frame Down 2
};


class Player : public iEntity
{
public:
 
    Player();
    ~Player()
    {}

    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void SetMode(StageOneMode& mode) { ownerMode_ = &mode; }

    //Reset the player variables and set it back to starting position
    void ResetVariables() override;
    //Upgrade the player ship using char key (s = Speed, w = Weapon).
    void UpgradeShip(char aspect);

    void ProcessUpgrade();

private:
    struct PlayerVariables
    {
        short unsigned int weaponLevel = 0;
        short unsigned int speedLevel = 0;
        short unsigned int bombCount = 0;
        short unsigned int speedMod = 0;

    };
    //Player thrust sprite
    Sprite thrusterSpr_;
    //Timers
    float thrustTimer_ = 0;
    float shotTimer_ = 0;
    //flags
    InputFlags inputFlags_;
    DirectionFlags dirFlags_;
    //States
    PlayerVariables pVars_;
    //What mode does this sprite belong to
    StageOneMode* ownerMode_ = nullptr;
                                     
    //Initialise block
    void Init() override;
    void InitPlayer();
    void InitThruster();
    //Update the player using keyboard flags
    bool UpdateKeyboard(float dTime);
    //Update the player using gamepad flags
    bool UpdateGamepad(float dTime);
    //Fire weapon script
    void FireWeapon(const std::type_info& projectileType);
    //Validate that the player is in the allowed playarea
    void ValidatePlayerPosition();
    //Handle and update on player inputs
    void UpdateInputs(float dTime);
    //Update animations
    void UpdateAnimations(float dTime);
    //Flag input triggers on keyboard/gamepad inputs
    void GetInputEvents();
    //Update animation flags
    void UpdateSpriteAnimationFlags(const Vector2& pos);
};
