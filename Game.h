#pragma once
#include <d3d11.h>             //DX11 Core
#include <cassert>             //Dubug asserts

#include "D3D.h"               //D3D Setup
#include "Singleton.h"         //Singleton design template

#include "SpriteBatch.h"       //DX
#include "SpriteFont.h"        //DX

#include "Input.h"             //Mouse/Keyboard/Gamepad input manager
#include "CollisionManager.h"  //Basic collision manager
#include "ModeManager.h"       //Game Mode/State manager
#include "ScoreManager.h"      //Score Manager
#include "AudioMgrFMOD.h"      //Audio
#include "FlagSets.h"          //Generic flags

using namespace DirectX;
using namespace DirectX::SimpleMath;

//Possible game states
enum class GameState
{ PLAY, PAUSE, GAME_OVER_SCREEN,
  SPLASH_SCREEN, START_SCREEN,
  INTRO_SCREEN, STAGE_ONE, STAGE_TWO,
  TEST_STAGE, MAIN_MENU_SCREEN,
  HIGH_SCORES};

class Game : public Singleton<Game>
{
public:

    Game(MyD3D& d3d)
        :d3d_(d3d), sprBatch_(nullptr), gameState_(GameState::SPLASH_SCREEN)
    { InitGame(); }

    //Useful accessor for clients that need D3D (like game objects)
    MyD3D& GetD3D() { return(d3d_); }

    //Release Resources
    void ReleaseGame();

    //Update variables and game logic
    void Update(float dTime);

    //Render sprites etc
    void Render(float dTime);

    //Check for inputs, update game accordingly
    void ProcessKey(char key) { modeManager_.ProcessKey(key); }

    void SetGameState(GameState newState) { gameState_ = newState; }

    //Get Managers & Utils
    MouseAndKeys& GetMK() { return mkInput_; }
    Gamepads& GetGP() { return gpInput_; }
    ModeManager& GetModeManager() { return modeManager_; }
    CollisionManager& GetCollisionManager() { return colManager_; }
    GameState& GetCurrentState() { return gameState_; }
    ScoreManager& GetScoreManager() { return scoreManager_; }

    //Get Flags
    GenericFlags16& GetGlobalFlags() { return globalFlags_; }

private:

    //Gamestates
    GameState gameState_;

    //Global Flags
    GenericFlags16 globalFlags_;

    //D3D object
    MyD3D& d3d_;

    //Managers
    ModeManager modeManager_;
    CollisionManager colManager_;
    Gamepads gpInput_;
    MouseAndKeys mkInput_;
    ScoreManager scoreManager_;

    //Hold channel handle
    unsigned int musicHandle;

    //Spritebatch & Spritefont resources
    SpriteBatch* sprBatch_;

    //Setup game, called in construction
    void InitGame();
};