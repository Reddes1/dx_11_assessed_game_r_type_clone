#include "SplashMode.h"         //Owner
#include "GameConstants.h"      //consts namespaces

SplashMode::SplashMode()
{
    //Reserve array memory
    splashes_.reserve(8);

    //Setup the background
    InitBackground();                       
}

SplashMode::~SplashMode()
{
    Release();
}

void SplashMode::Release()
{
    //Clear splash array
    splashes_.clear();
}

void SplashMode::Enter()
{
    //Update Game State
    Game::Get().SetGameState(GameState::SPLASH_SCREEN);
}

void SplashMode::Update(float dTime)
{
    //Switch mode when time passes or user inputs
    if (MC::SPLASH_1_DURATION < GetClock() || Game::Get().GetMK().IsPressed(VK_SPACE) ||
        Game::Get().GetGP().IsPressed(0, XINPUT_GAMEPAD_START))
    {
        Game::Get().GetModeManager().SwitchMode("INTRO");
    }
}

void SplashMode::Render(float dTime, DirectX::SpriteBatch& batch)
{
    for (auto& s : splashes_)
    {
        s.Draw(batch);
    }
}

void SplashMode::ProcessKey(char key)
{
    //Maybe ESC code here.
}

void SplashMode::InitBackground()
{
    assert(splashes_.empty());
    
    //Handy Handle
    Game& game = Game::Get();

    //Insert sprites
    splashes_.insert(splashes_.begin(), FC::SPLASH_BG_COUNT, Sprite(game.GetD3D()));

    //Setup filepaths and nicknames
    pair<std::string, std::string> files[FC::SPLASH_BG_COUNT]
    {
        {"Splash 1","data/backgrounds/splashes/splashscreenbg1.dds"}
    };

    //Load and set textures
    size_t i = 0;
    for (auto& f : files)
    {
        ID3D11ShaderResourceView* p = Game::Get().GetD3D().GetCache().LoadTexture(&game.GetD3D().GetDevice()
            , f.second, f.first, false);
        if (!p)
            assert(false);
        splashes_[i++].SetTexture(*p);
    }
    
    //Scale sprites to screen size
    int width = WinUtil::Get().GetData().clientWidth;
    int height = WinUtil::Get().GetData().clientHeight; 
    for (size_t i(0); i < splashes_.size(); i++)
    {
        splashes_[i].SetScale({ width / splashes_[i].GetTextureData().dim.x,
            height / splashes_[i].GetTextureData().dim.y });
    }

}

