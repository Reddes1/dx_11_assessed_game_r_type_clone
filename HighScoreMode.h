#pragma once

#include "Game.h"            //Game Manager
#include "ModeInterface.h"   //Mode Parent
#include "Sprite.h"          //For making sprite vector arrays
#include "SpriteFont.h"      //For screen text



/*
Takes information from the score manager and displays it.
*/

enum class MODE_STATES
{
    PLAYER_INPUT, DISPLAY_SCORES
};


class HighScoreMode : public iMode
{
public:
    //HMode Handle
    inline static const std::string MODE_NAME = "HIGH_SCORES";

    HighScoreMode();
    ~HighScoreMode();

    //Parent overrides
    void Enter() override;
    bool Exit() override;
    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ProcessKey(char key) override;
    std::string GetModeName() const override { return MODE_NAME; }


private:
    //Background sprites
    std::vector<Sprite> backgrounds_;

    //Fonts
    DirectX::SpriteFont* font1_;    //Algerian

    //Temporary container our player name
    std::string tempName_;
    //Mode state
    MODE_STATES state;

    //Initialise
    void InitBackground();
    //Render Switch
    void RenderManager(DirectX::SpriteBatch& batch);
    //Render the input state
    void RenderInput(DirectX::SpriteBatch& batch);
    //Render the highscores state
    void RenderScores(DirectX::SpriteBatch& batch);
    //Add the the player to table using ScoreManager interfaces
    void AddTableEntry();
    //Add character to namestring na run validation checks
    void AddChar(char character);
    //Release resources
    void Release();
};