#include "PauseMode.h"

#include "GameConstants.h"      //consts namespaces

PauseMode::PauseMode()
{
    //Allocate Memory & Reserve vector space
    font1_ = new SpriteFont(&Game::Get().GetD3D().GetDevice(), L"data/fonts/algerian.spritefont");
    backgrounds_.reserve(2);

    //Inits
    InitBackground();
}

PauseMode::~PauseMode()
{
    Release();
}

void PauseMode::Enter()
{
    //Update our game state
    Game::Get().SetGameState(GameState::PAUSE);

}

bool PauseMode::Exit()
{
    //If heading to main menu, stop current track
    if(flags_.flag1)
        AudioMgrFMOD::Get().GetSongMgr()->Stop();

    return true;
}

void PauseMode::Update(float dTime)
{
    //Handy Handle
    Game& game = Game::Get();
    if(game.GetGP().IsConnected(0))
    {
        if (game.GetMK().IsPressed(VK_SPACE) || game.GetGP().IsPressed(0, XINPUT_GAMEPAD_A))
        {
            ProcessMoveToGame();
        }
        else if (game.GetMK().IsPressed(VK_ESCAPE) || game.GetGP().IsPressed(0, XINPUT_GAMEPAD_B))
        {
            ProcessMoveToMainMenu();
        }
    }

}

void PauseMode::ProcessMoveToMainMenu()
{
    //Flag internally that we are leaving to the menu
    flags_.flag1 = true;
    //Need to let StageOne know that we are going to reenter from main menu, so use a global flag
    Game::Get().GetGlobalFlags().flag1 = true;
    Game::Get().GetModeManager().SwitchMode("MAIN_MENU");
}
void PauseMode::ProcessMoveToGame()
{
    //Disable flag, so game mode knows that we are entering from pause
    Game::Get().GetGlobalFlags().flag1 = false;
    Game::Get().GetModeManager().SwitchMode("STAGE_ONE");

}

void PauseMode::Render(float dTime, DirectX::SpriteBatch& batch)
{
    //Render background
    for (auto& a : backgrounds_)
    {
        a.Draw(batch);
    }

    //Render Text
    RenderText(batch);
}

void PauseMode::ProcessKey(char key)
{
    switch (key)
    {
    case(VK_SPACE):
        ProcessMoveToGame();
        break;
    case(VK_ESCAPE):
        ProcessMoveToMainMenu();
        break;
    }
}

void PauseMode::InitBackground()
{
    assert(backgrounds_.empty());
    //Handy Handles
    Game& game = Game::Get();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Insert sprite and load texture
    backgrounds_.insert(backgrounds_.begin(), FC::GAME_OVER_BG_COUNT, Sprite(game.GetD3D()));
    ID3D11ShaderResourceView* p = Game::Get().GetD3D().GetCache().LoadTexture(&game.GetD3D().GetDevice()
        , "data/backgrounds/backgroundatlas1.dds", "Background Atlas 1", false);


    //Configure sprite
    backgrounds_[0].SetTexture(*p);
    backgrounds_[0].SetTextureRect(FD::backgroundAtlasFrames[4]);
    backgrounds_[0].SetScale(Vector2(width / (FD::backgroundAtlasFrames[0].right - FD::backgroundAtlasFrames[0].left),
        height / (FD::backgroundAtlasFrames[0].bottom - FD::backgroundAtlasFrames[0].top)));
}

void PauseMode::RenderText(DirectX::SpriteBatch& batch)
{
    //Handy Handles
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    std::string message = "Space/A to Continue, Esc/B to Quit to Menu";

    Vector2 pos{ width * 0.5f, height * 0.8f };
    RECT textDim = font1_->MeasureDrawBounds(message.c_str(), Vector2(0, 0));

    font1_->DrawString(&batch, message.c_str(), pos, Colours::White, 0, Vector2((float)textDim.right * 0.5f, 1));
}

void PauseMode::Release()
{
    backgrounds_.clear();

    delete font1_;
    font1_ = nullptr;
}
