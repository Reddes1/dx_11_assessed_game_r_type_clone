#include "Game.h"

//Utils
#include "D3DUtil.h"        //D3D Utils and constants
#include "WindowUtils.h"    //Window Utils

//DX11 Headers
#include "CommonStates.h"   //DX

//Modes
#include "SplashMode.h"     //Dev Splash
#include "IntroMode.h"      //Intro Mode
#include "MainMenuMode.h"   //Main Menu
#include "StageOneMode.h"   //First game stage
#include "GameOverMode.h"   //Game Over
#include "HighScoreMode.h"  //Highscore table
#include "PauseMode.h"      //Pause screen
#include "HowToPlayMode.h"  //HowToPlay screen

void Game::InitGame()
{
    //Handy Handles
    AudioMgrFMOD& audio = AudioMgrFMOD::Get();

    //Initialise KM/Pad input control
    mkInput_.Initialise(WinUtil::Get().GetData().hMainWnd, true, false);
    gpInput_.Initialise();

    //Setup spritebatch & text objects
    sprBatch_ = new SpriteBatch(&d3d_.GetDeviceCtx());
    assert(sprBatch_);

    //Add gamemodes
    modeManager_.AddMode(new SplashMode());
    modeManager_.AddMode(new IntroMode());
    modeManager_.AddMode(new MainMenuMode());
    modeManager_.AddMode(new StageOneMode());
    modeManager_.AddMode(new GameOverMode());
    modeManager_.AddMode(new HighScoreMode());
    modeManager_.AddMode(new PauseMode());
    modeManager_.AddMode(new HowToPlayMode());

    //Set initial mode
    modeManager_.SwitchMode(SplashMode::MODE_NAME);
}

void Game::ReleaseGame()
{
    delete sprBatch_;
    sprBatch_ = nullptr;
}

void Game::Update(float dTime)
{
    //Handy Handles
    AudioMgrFMOD& audio = AudioMgrFMOD::Get();

    //Update pad 
    gpInput_.Update();

    //Render current mode
    modeManager_.Update(dTime);

    //Play track based on state
    switch (gameState_)
    {
    case(GameState::STAGE_ONE): //Play Track 1 Here, and stop Track 0 if playing
    case(GameState::PAUSE):
        if (!audio.GetSongMgr()->IsPlaying(musicHandle))
            audio.GetSongMgr()->Play("track2", true, false, &musicHandle, 0.4f);
        break;

    default:  //Every other Mode here, Play Track 0, and stop Track 1 if playing
        if (!audio.GetSongMgr()->IsPlaying(musicHandle))
            audio.GetSongMgr()->Play("track1", true, false, &musicHandle, 0.4f);
        break;
    }
}

void Game::Render(float dTime)
{
    //Begin render cycle
    d3d_.BeginRender(Colours::Black);
    CommonStates dxstate(&d3d_.GetDevice());

    //Alpha removed batch
    sprBatch_->Begin(SpriteSortMode_Deferred, dxstate.NonPremultiplied(), &d3d_.GetSampler(true));

    //Render current mode
    modeManager_.Render(dTime, *sprBatch_);

    //Start render cycle
    sprBatch_->End();
    d3d_.EndRender();

    //Clean up relative mouse coords 
    mkInput_.PostProcess();
}

