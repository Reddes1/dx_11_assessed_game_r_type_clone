#include "MainMenuMode.h"       //Owner

#include "GameConstants.h"      //consts namespaces

MainMenuMode::MainMenuMode()
{   
    backgrounds_.reserve(2);

    //Inits
    InitBackground();
    InitMenu();
}

MainMenuMode::~MainMenuMode()
{
    //Delete menu object
    for (size_t i(0); i < menu_.size(); ++i)
    {
        delete menu_[i];
    }
    //Clear containers
    menu_.clear();
    backgrounds_.clear();
}

void MainMenuMode::Enter()
{
    //Update our game state
    Game::Get().SetGameState(GameState::MAIN_MENU_SCREEN);
}

void MainMenuMode::Update(float dTime)
{
    //Menu Interaction Here
    for (auto& m : menu_)
    {
        m->Update(dTime);
    }

}

void MainMenuMode::Render(float dTime, DirectX::SpriteBatch& batch)
{
    //Render background
    for (auto& a : backgrounds_)
    {
        a.Draw(batch);
    }
    
    //Render Menu
    for (auto& a : menu_)
    {
        a->Render(dTime, batch);
    }
}

void MainMenuMode::ProcessKey(char key)
{
    switch (key)
    {
    case VK_ESCAPE: //Exit out
        PostQuitMessage(0);
        break;
    }
}

void MainMenuMode::InitMenu()
{
    //Add new object in
    MainMenuObj* obj_ = new MainMenuObj();
    menu_.insert(menu_.begin(), 1, obj_);
}

void MainMenuMode::InitBackground()
{
    assert(backgrounds_.empty());
    //Handy Handle
    Game& game = Game::Get();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Insert sprite and load texture
    backgrounds_.insert(backgrounds_.begin(), FC::MAIN_MENU_BG_COUNT, Sprite(game.GetD3D()));
    ID3D11ShaderResourceView* p = Game::Get().GetD3D().GetCache().LoadTexture(&game.GetD3D().GetDevice()
        , "data/backgrounds/backgroundatlas1.dds", "Background Atlas 1", false);

    //Configure sprite
    backgrounds_[0].SetTexture(*p);
    backgrounds_[0].SetTextureRect(FD::backgroundAtlasFrames[1]);
    backgrounds_[0].SetScale(Vector2(width / (FD::backgroundAtlasFrames[1].right - FD::backgroundAtlasFrames[1].left),
        height / (FD::backgroundAtlasFrames[1].bottom - FD::backgroundAtlasFrames[1].top)));
}