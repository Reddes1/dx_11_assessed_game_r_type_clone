#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <string>
#include <sstream>
#include <assert.h>

#include "D3D.h"
#include "D3DUtil.h"
#include "Singleton.h" //Singleton pattern template

using namespace std;

//Debug measurement macros
#define DBOUT( s )            \
{                             \
   std::ostringstream os_;    \
   os_ << s << "\n";                   \
   OutputDebugString( os_.str().c_str() );  \
}

#define WDBOUT(s)				\
{								\
   std::wostringstream os_;		\
   os_ << s << L"\n";				       \
   OutputDebugStringW( os_.str().c_str() );  \
}

class MyD3D;

class WinUtil : public Singleton<WinUtil>
{
private:

    struct WinData
    {
        //all windows apps have these handles
        HINSTANCE hAppInst = 0;
        HWND      hMainWnd = 0;
        DWORD winStyle = (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX /*| WS_MAXIMIZEBOX*/);
        bool appPaused = false;
        bool minimized = false;
        bool maximized = false;
        bool resizing = false;
        std::string mainWndCaption;
        int clientWidth = 0;
        int clientHeight = 0;
        const int minClientWidth = 250;
        const int minClientHeight = 200;
    };

    WinData windowData_;
    MyD3D* d3d_;

    //handle messages from the operating system
    LRESULT DefaultMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);


public:
    WinUtil()
        :d3d_(nullptr)
    {}

    //Message pump handler
    static LRESULT GetDefaultMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
    {
        return WinUtil::Get().DefaultMssgHandler(hwnd, msg, wParam, lParam);
    }

    //setup a window to render into
    bool InitMainWindow(int width, int height, HINSTANCE hInstance, const std::string& appName, WNDPROC mssgHandler, bool centerScrn);
    
    //Choose window resolution from value array
    void ChooseRes(int& w, int& h, int defaults[], int numPairs);
    
    int Run(void(*pUpdate)(float), void(*pRender)(float));
    bool BeginLoop(bool& canUpdateRender);
    float EndLoop(bool didUpdateRender);

    //Set D3D reference
    void SetD3D(MyD3D& d3d)
    {
        assert(d3d_ == nullptr);
        d3d_ = &d3d;
    }

    //Return client data (update later)
    const WinData& GetData() const
    {
        return windowData_;
    }


};
