#pragma once
#include <string>
#include <fstream>      //File accessor
#include <vector>

//Basic name and score container, with overloaded operators to interact with the score data.
struct ScoreContainer
{
    std::string name;
    int score = 0;

    ScoreContainer& operator+(int& rValue)
    {
        score = score + rValue;
    }
    ScoreContainer& operator-(int& rValue)
    {
        score = score - rValue;
    }
    ScoreContainer& operator*(int& rValue)
    {
        score = score * rValue;
    }
    bool operator<(int& rValue)
    {
        return (score < rValue);
    }
    bool operator>(int& rValue)
    {
        return (score > rValue);
    }
    bool operator<=(int& rValue)
    {
        return (score <= rValue);
    }
    bool operator>=(int& rValue)
    {
        return (score >= rValue);
    }
    ScoreContainer& operator+=(const int& rValue)
    {
        score += rValue;
        return *this;
    }
    ScoreContainer& operator-=(const int& rValue)
    {
        score -= rValue;
    }
    ScoreContainer& operator=(ScoreContainer& rSide)
    {
        name = rSide.name;
        score = rSide.score;
        return *this;
    }
};


/*
    Score manager that holds the currently active players name and score, open and sort data from file. 
*/
class ScoreManager
{
public:

    ScoreManager();
    ~ScoreManager();

    //Return the data type containing both name and score at highscore table index
    const ScoreContainer& GetTableEntry(int index) const { return scoresTable_[index]; }
    //Return player name at highscore table index
    const std::string GetPlayerNameFromTable(int index) const { return scoresTable_[index].name; }
    //Return player score at highscore table index
    const int GetPlayerScoreFromTable(int index) const { return scoresTable_[index].score; }

    //Return the active player data
    const ScoreContainer& GetActivePlayerData() const { return currentPlayerData_; }
    //Return the active players name
    const std::string GetActivePlayerName() const { return currentPlayerData_.name; }  
    //Return the active players score 
    const int GetActivePlayerScore() const { return currentPlayerData_.score; }

    //Set the active players name
    void SetActivePlayerName(std::string& name) { currentPlayerData_.name = name; }
    //Set the active player score to specific value
    void SetActivePlayerScore(int newValue) { currentPlayerData_.score = newValue; }
    //Update the player score with a int value
    void UpdateActivePlayerScore(int scoreIncrease) { currentPlayerData_ += scoreIncrease; }

    //Add the current player into highscore table & automatically sort it.
    void AddActivePlayerToTable();

    //Output the scores tables to file in the desired format.
    void SaveDataToFile();

private:
    
    //Hold the table data in here
    std::vector<ScoreContainer> scoresTable_;
    //Current active player score.
    ScoreContainer currentPlayerData_;

    //Load and seperate file data into scores table.
    void LoadFileDataIntoTable();
    //Sort the scores table into highest to lowest
    void SortHighScoreTable();
    //Create a new table file with data if we can't find the table
    void CreateNewTableData();
    //Clean up
    void Release();
};