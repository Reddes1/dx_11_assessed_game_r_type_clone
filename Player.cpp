#include "Player.h"         //Header
#include "Game.h"           //Game Manager
#include "AudioMgrFMOD.h"   //Audio Manager

#include "GameConstants.h"  //Const values

#include "PlayerProjectiles.h"   //Projectiles

Player::Player()
    : iEntity(Game::Get().GetD3D()),
    thrusterSpr_(Game::Get().GetD3D())
{
    Init();
}
void Player::Init()
{
    InitPlayer();
    InitThruster();
}

void Player::InitPlayer()
{
    //Handy Handles
    MyD3D& d3d = Game::Get().GetD3D();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    ////Load and setup object
    //ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "data/sqrTest.dds", "Ship Texture", false);

    //Calulate and capture adjustments for screen size
    adj_.scaleAdj = Vector2(width / FC::DEFAULT_RES.x, height / FC::DEFAULT_RES.y);
    adj_.spdAdj = Vector2(width / FC::DEFAULT_RES.x, height / FC::DEFAULT_RES.y);

    //Load texture and set frames
    std::vector<RECTF> frames(shipAnimFrames,
        shipAnimFrames + sizeof(shipAnimFrames) / sizeof(shipAnimFrames[0]));
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "data/sprites/player/playersheet.dds",
        "Player Atlas 1", false, &frames);

    //Set object parameters
    spr_.SetTexture(*p);
    spr_.SetScale(Vector2(adj_.scaleAdj.x * GC::P_SHIP_SCALE, adj_.scaleAdj.y * GC::P_SHIP_SCALE));
    spr_.SetOrigin(Vector2((frames[0].right - frames[0].left) * 0.5f, (frames[0].bottom - frames[0].top) * 0.5f));
    spr_.SetPosition(Vector2(width * 0.2f, height * 0.5f));

    //Set animation parameters
    spr_.GetAnimation().Initialise(0, 4, 4, false);
    spr_.GetAnimation().SetFrame(2);
    spr_.GetAnimation().Play(false);
}

void Player::InitThruster()
{
    //Handy Handles
    MyD3D& d3d = Game::Get().GetD3D();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Load thruster texture and frames
    std::vector<RECTF> frames(thrustAnimFrames, thrustAnimFrames + sizeof(thrustAnimFrames) / sizeof(thrustAnimFrames[0]));
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "data/thrust.dds", "Thrust Texture", false, &frames);

    //Set thruster parameters
    thrusterSpr_.SetTexture(*p);
    thrusterSpr_.SetOrigin(Vector2(thrustAnimFrames[0].right * 0.5f,
        thrustAnimFrames[0].bottom * 0.5f));
    thrusterSpr_.SetScale(Vector2(GC::P_SHIP_SCALE, GC::P_SHIP_SCALE));
    thrusterSpr_.GetAnimation().Initialise(0, 3, 15, true);
    thrusterSpr_.GetAnimation().Play(true);
    thrusterSpr_.SetRotation(PI / 2.f);
}

void Player::Update(float dTime)
{
    //No need to update if not active currently
    if (isActive_)
    {
        //Get and flag inputs
        GetInputEvents();
        //Update player action using flags
        UpdateInputs(dTime);
        //Update animation
        UpdateAnimations(dTime);
    }
}

void Player::Render(float dTime, DirectX::SpriteBatch& batch)
{
    //No special rendering, so let parent do it
    iEntity::Render(dTime, batch);

    if (thrustTimer_ > GetClock())
        thrusterSpr_.Draw(batch);
}

void Player::ResetVariables()
{
    //Handy Handle
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Reset Position
    spr_.SetPosition(Vector2(width * 0.2f, height * 0.5f));
    //Reset variables
    thrustTimer_ = 0;
    shotTimer_ = 0;
    memset(&pVars_, 0, sizeof(pVars_));
}

void Player::GetInputEvents()
{
    //Handy Handles
    Game& game = Game::Get();

    //Check that any of the keys or buttons we are looking for have been pressed and flag them.    
    //Directional Flags
    inputFlags_.isUpPressed = game.GetMK().IsPressed(VK_UP) || game.GetMK().IsPressed(VK_W);
    inputFlags_.isDownPressed = game.GetMK().IsPressed(VK_DOWN) || game.GetMK().IsPressed(VK_S);
    inputFlags_.isLeftPressed = game.GetMK().IsPressed(VK_LEFT) || game.GetMK().IsPressed(VK_A);
    inputFlags_.isRightPressed = game.GetMK().IsPressed(VK_RIGHT) || game.GetMK().IsPressed(VK_D);

    //Play fire flags
    inputFlags_.isAction1Pressed = game.GetMK().IsPressed(VK_SPACE) || game.GetMK().IsPressed(VK_J);

    //Check gamepad inputs
    if (game.GetGP().IsConnected(0))
    {
        inputFlags_.isLeftStickMoved = game.GetGP().GetState(0).leftStickX != 0 || game.GetGP().GetState(0).leftStickY != 0;

        //If we didn't positive before, check again
        if (!inputFlags_.isAction1Pressed)
            inputFlags_.isAction1Pressed = game.GetGP().IsPressed(0, XINPUT_GAMEPAD_A);
        
    }
}

void Player::UpdateSpriteAnimationFlags(const Vector2& pos)
{
    //Compare old and new position to see what direction we are using and set flags
    if (pos.x < spr_.GetPosition().x)
        dirFlags_.movingLeft = true;
    else if (pos.x > spr_.GetPosition().x)
        dirFlags_.movingRight = true;
    if (pos.y < spr_.GetPosition().y)
        dirFlags_.movingUp = true;
    else if (pos.y > spr_.GetPosition().y)
        dirFlags_.movingDown = true;
}

bool Player::UpdateKeyboard(float dTime)
{
    //Handy Handles
    Game& game = Game::Get();

    //Check for any keyboard inputs for player movement
    bool isKeyPressed = inputFlags_.isUpPressed || inputFlags_.isDownPressed
        || inputFlags_.isLeftPressed || inputFlags_.isRightPressed;

    //If no registered key was pressed, early out
    if (!isKeyPressed)
        return false;
    else
    {
        //Capture any position changes here
        Vector2 pos(0, 0);

        //Move player based on flags
        if (inputFlags_.isUpPressed)              //Up
            pos.y -= (GC::P_KB_SPEED + pVars_.speedMod) * adj_.spdAdj.y * dTime;
        else if (inputFlags_.isDownPressed)       //Down
            pos.y += (GC::P_KB_SPEED + pVars_.speedMod) * adj_.spdAdj.y * dTime;
        if (inputFlags_.isLeftPressed)            //Left
            pos.x -= (GC::P_KB_SPEED + pVars_.speedMod) * adj_.spdAdj.x * dTime;
        else if (inputFlags_.isRightPressed)      //Right
            pos.x += (GC::P_KB_SPEED + pVars_.speedMod) * adj_.spdAdj.x * dTime;

        //Add new position to player
        pos += spr_.GetPosition();
        //Update our animation flags before updating position
        UpdateSpriteAnimationFlags(pos);
        //Set our final pos for frame
        spr_.SetPosition(pos);

        //Update thrust timer
        thrustTimer_ = GetClock() + GC::P_THRUSTER_DURATION;
        //Handle any broad flags
        dirFlags_.isMoving = true;
        //Validate the player position (is it still in the play area)
        ValidatePlayerPosition();

        return true;
    }

    return false;
}
bool Player::UpdateGamepad(float dTime)
{
    //Handy Handles
    Game& game = Game::Get();

    if (!inputFlags_.isLeftStickMoved)
        return false;
    else
    {
        //Capture any position changes here
        Vector2 pos(0, 0);

        //Update position
        pos.x += game.GetGP().GetState(0).leftStickX * (GC::P_KB_SPEED + pVars_.speedMod) * adj_.spdAdj.x * dTime;
        pos.y -= game.GetGP().GetState(0).leftStickY * (GC::P_KB_SPEED + pVars_.speedMod) * adj_.spdAdj.y * dTime;

        //Add new position to player
        pos += spr_.GetPosition();
        //Update our animation flags before updating position
        UpdateSpriteAnimationFlags(pos);
        //Set our final pos for frame
        spr_.SetPosition(pos);

        //Update thrust timer
        thrustTimer_ = GetClock() + GC::P_THRUSTER_DURATION;
        //Handle any broad flags
        dirFlags_.isMoving = true;
        //Validate the player position (is it still in the play area)
        ValidatePlayerPosition();
               
        return true;
    }

    return false;
}

void Player::FireWeapon(const std::type_info& projectileType)
{
    //Handy Handles
    AudioMgrFMOD& audio = AudioMgrFMOD::Get();

    //Grab first free projectile
    iEntity* proj = ownerMode_->FindFirstProjectile(projectileType, false);
    if (proj)
    {
        //Setup parameters
        proj->SetIsActive(true);
        proj->GetSprite().SetPosition(Vector2(spr_.GetPosition().x + spr_.GetScreenSize().x * 0.5f,
            spr_.GetPosition().y));
        proj->GetSprite().SetVelocity(Vector2(1, 0));
        //Set next shot delay
        shotTimer_ = GetClock() + GC::P_SHOT_DELAY;

        //Play SFX
        audio.GetSfxMgr()->Play("LaserSFX", false, false, nullptr, 0.05f);
    }
}

void Player::UpgradeShip(char upgradeType)
{
    upgradeType = std::tolower(upgradeType);
    switch (upgradeType)
    {
    case('s'):
        if (pVars_.speedLevel < GC::P_MAX_SPEED_LEVEL)
            ++pVars_.speedLevel;
        ProcessUpgrade();
        break;
    case('w'):
        if (pVars_.weaponLevel < GC::P_MAX_WEAPON_LEVEL)
            ++pVars_.weaponLevel;
        break;
    case('b'):
        //Bomb code here
        break;
    }
}
void Player::ProcessUpgrade()
{
    switch (pVars_.speedLevel)
    {
    case(1):
        pVars_.speedMod = GC::P_SPEED_LV_1;
        break;
    case(2):
        pVars_.speedMod = GC::P_SPEED_LV_2;
        break;
    default:
        pVars_.speedMod = 0;
    }
}

void Player::ValidatePlayerPosition()
{
    //Capture play position
    Vector2 pos = spr_.GetPosition();

    //Define playarea (like the screen)
    RECTF playArea = ownerMode_->GetPlayArea();

    //Check player position against our play area limitations
    if (pos.x < playArea.left)         //Left side
        pos.x = playArea.left;
    else if (pos.x > playArea.right)   //Right
        pos.x = playArea.right;
    if (pos.y < playArea.top)          //Top
        pos.y = playArea.top;
    else if (pos.y > playArea.bottom)  //Bottom
        pos.y = playArea.bottom;

    //Set new position
    spr_.SetPosition(pos);
}

void Player::UpdateInputs(float dTime)
{
    //Handy References
    Game& game = Game::Get();

    //Check each input method, if one returns true for an update then skip updating through the others
    if(UpdateKeyboard(dTime))       //Check for keyboard inputs first
    { }
    else if (UpdateGamepad(dTime))  //Then check controller
    { }
    else                            //No inputs, do any null input changes here
    {
        //Let later functions know the player isnt moving
        dirFlags_.isMoving = false;
    }

    //If timer allows, fire weapon
    if (inputFlags_.isAction1Pressed && GetClock() > shotTimer_)
    {
        //Determine what type of projectile we are firing
        switch (pVars_.weaponLevel)
        {
        case(0):
            FireWeapon(typeid(PlayerProjectile1));
            break;
        case(1):
            FireWeapon(typeid(PlayerProjectile2));
            break;
        default:
            FireWeapon(typeid(PlayerProjectile1));
        }
    }
}

void Player::UpdateAnimations(float dTime)
{
    //Set animation direction for the direction the player is moving and play the animation
    if (dirFlags_.movingUp)
    {
        spr_.GetAnimation().SetAnimationReverse(true);
        spr_.GetAnimation().Play(true);
    }
    else if (dirFlags_.movingDown)
    {
        spr_.GetAnimation().SetAnimationReverse(false);
        spr_.GetAnimation().Play(true);
    }

    /*
        If the player is no longer moving up or down, we want the ship to move back to the default frame, and not be stuck in its current frame till
        given another input. Here we determine the current frame and then forward/reverse the animation till finding our default frame.
    */
    if (!dirFlags_.movingUp && !dirFlags_.movingDown)
    {
        int currentFrame = spr_.GetAnimation().GetCurrentFrame();

        switch (currentFrame)
        {
        case(2):    //If we are already at the neutral frame, pause the sprite animation and break
            spr_.GetAnimation().Play(false);
            break;

        case(0):    //Reverse to moving up sprite
        case(1):
            spr_.GetAnimation().SetAnimationReverse(false);
            break;
        case(3):
        case(4):
            spr_.GetAnimation().SetAnimationReverse(true);
            break;
        }
    }

    //Reset flags for next frame
    dirFlags_.movingUp = false;
    dirFlags_.movingDown = false;

    //Update the animation
    spr_.GetAnimation().Update(dTime);

    //If timer isn't 0
    if (thrustTimer_)
    {
        //Update the thruster position the the current play position with an x offset.
        thrusterSpr_.SetPosition(Vector2(spr_.GetPosition().x - (spr_.GetScreenSize().x * 0.7f), spr_.GetPosition().y + (spr_.GetScreenSize().y * 0.1f)));

        thrusterSpr_.GetAnimation().Update(dTime);
    }
}