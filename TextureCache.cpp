#include "TextureCache.h"


void TexCache::Release()
{
    //Uses the type pair(defines first/second) to clear the mapped value (the second)
    for (auto& pair : mCache_)
        ReleaseCOM(pair.second.pTex);
    //Clear the rest of the cached data.
    mCache_.clear();
}

//C++ 17 Required for filesystem
ID3D11ShaderResourceView* TexCache::LoadTexture(ID3D11Device* pDevice, const std::string& fileName,
    const std::string& texName, bool appendPath)
{
    //If no nickname or texture name given, find texture through filepath and give new name
    std::string name = texName;
    if (name.empty())
    {
        std::filesystem::path p(fileName);
        name = p.stem().string();
    }

    //Search cache for texture
    TexMap::iterator iter = mCache_.find(name);
    //If the iterator found the texture being looked for
    if (iter != mCache_.end())
        return (*iter).second.pTex;

    //For path manipulation
    const std::string* tempPath = &fileName;

    //If appending a file path struture 
    if (appendPath)
    {
        std::string path = assetPathAppend_ + fileName;
        tempPath = &path;
    }
    //Set load path from manipulated path
    std::wstring compFilePath(tempPath->begin(), tempPath->end());

    //Load Texture
    DDS_ALPHA_MODE alpha;
    ID3D11ShaderResourceView* pT = nullptr;

    //If load fails
    if (CreateDDSTextureFromFile(pDevice, compFilePath.c_str(), nullptr, &pT, 0, &alpha) != S_OK)
    {
        WDBOUT(L"Cannot load " << compFilePath << L"\n");
        assert(false);
        return false;
    }

    //Save into cache
    assert(pT);
    mCache_.insert(TexMap::value_type(name, Data(fileName, pT, GetDimensions(pT))));

    return pT;
}

//Return Vec2 Dimensions
Vector2 TexCache::GetDimensions(ID3D11ShaderResourceView* pTex)
{
    assert(pTex);
    //Get the texture resource
    ID3D11Resource* res = nullptr;
    pTex->GetResource(&res);
    assert(res);
    //Query
    ID3D11Texture2D* tex2D = nullptr;
    HRESULT hr = res->QueryInterface(&tex2D);
    //Return vector
    Vector2 dim(0, 0);
    //If query worked
    if (SUCCEEDED(hr))
    {
        //Get a description of the texture file and set values from it
        D3D11_TEXTURE2D_DESC desc;
        tex2D->GetDesc(&desc);
        dim = { static_cast<float>(desc.Width), static_cast<float>(desc.Height) };
    }
    //Release resources
    ReleaseCOM(tex2D);
    ReleaseCOM(res);
    return dim;
}

//Get texture slowly
TexCache::Data& TexCache::Get(ID3D11ShaderResourceView* pTex) 
{
    //Iterate through cache
    TexMap::iterator iter = mCache_.begin();
    //Return pointer
    Data* p = nullptr;
    //If not at end and pointer not null
    while (iter != mCache_.end() && !p)
    {
        //if iterator finds texture
        if ((*iter).second.pTex == pTex)
            p = &(*iter).second;
        ++iter;
    }
    assert(p);
    return *p;
}