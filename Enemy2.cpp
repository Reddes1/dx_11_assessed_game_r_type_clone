#include "Enemy2.h"
#include "EnemyProjectile1.h"

#include "GameConstants.h"  //Frame data and value constants

Enemy2::Enemy2()
    :iEntity(Game::Get().GetD3D())
{
    Init();
}

void Enemy2::Init()
{
    //Handy Handles
    MyD3D& d3d = Game::Get().GetD3D();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Load texture and set frames
    std::vector<RECTF> frames(FD::stageOneEnemyAtlasFrames,
        FD::stageOneEnemyAtlasFrames + sizeof(FD::stageOneEnemyAtlasFrames) / sizeof(FD::stageOneEnemyAtlasFrames[0]));
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "data/sprites/stageone/stageoneatlas.dds",
        "Stage One Atlas 1", false, &frames);

    //Calulate and capture adjustments for screen size
    adj_.scaleAdj = Vector2(width / FC::DEFAULT_RES.x, height / FC::DEFAULT_RES.y);
    adj_.spdAdj = Vector2(width / FC::DEFAULT_RES.x, height / FC::DEFAULT_RES.y);

    //Set Object parameters
    spr_.SetTexture(*p);
    spr_.SetOrigin(Vector2((frames[8].right - frames[8].left) * 0.5f, (frames[8].bottom - frames[8].top) * 0.5f));
    spr_.SetScale(Vector2(adj_.scaleAdj.x * GC::E2_SCALE, adj_.scaleAdj.y * GC::E2_SCALE));
    spr_.GetAnimation().Initialise(8, 13, 8, true);
    spr_.GetAnimation().Play(true);
    health_ = GC::E2_STARTING_HP;
}

void Enemy2::Update(float dTime)
{
    //If HP = 0, and flagged that the object just died.
    if (health_ <= 0 && !flags1_.isDead && isActive_)
    {
        flags1_.isDead = true;
    }

    //Check if object just died, else if check active state
    if (flags1_.isDead)
    {
        isActive_ = false;
        flags1_.isDead = false;
        Game::Get().GetScoreManager().UpdateActivePlayerScore(12);
    }
    //Update if active
    if (isActive_)
    {
        Vector2 pos = spr_.GetPosition();
        //Calculate new position
        pos += spr_.GetVelocity() * (GC::E2_SPEED * adj_.spdAdj) * dTime;
        //Set new posiiton
        spr_.SetPosition(pos);

        //Become inactive when offscreen
        if (spr_.GetPosition().x < 0 - spr_.GetScreenSize().x)
        {
            isActive_ = false;
        }

        //Process Shooting
        if (GetClock() > shotTimer_)
        {
            FireProjectile();
        }
        spr_.GetAnimation().Update(dTime);
    }
}

void Enemy2::Render(float dTime, DirectX::SpriteBatch& batch)
{
    //No special rendering, let the parent handle it
    iEntity::Render(dTime, batch);
}

void Enemy2::FireProjectile()
{
    //Grab a free projectile
    iEntity* proj = ownerMode_->FindFirstProjectile(typeid(EnemyProjectile1), false);
    iEntity* player = ownerMode_->FindFirstObject(typeid(Player), true);
    if (proj && player)
    {
        //Capture current position
        Vector2 pos = spr_.GetPosition();
        //Set projectile to enemy position
        proj->GetSprite().SetPosition(pos);
        //Set our target (the player)
        Vector2 target_ = player->GetSprite().GetPosition();
        //Determine a direction
        Vector2 direction = target_ - pos;
        //Normalise and pass to projectile sprite
        direction.Normalize();
        //Set the velocity on the sprite
        proj->GetSprite().SetVelocity(direction);
        //Set our shot delay
        shotTimer_ = GetClock() + GC::E2_SHOT_DELAY + GetRandom(1, 4);

        proj->SetIsActive(true);
    }
}

void Enemy2::ResetVariables()
{
    health_ = GC::E2_STARTING_HP;
    spr_.SetVelocity(Vector2(-1, 0));
}

