#pragma once                    
#include "EntityInterface.h"    //Parent Object

using namespace DirectX;

//Animation frames
const RECTF missileSpin[]{
 { 0,  0, 53, 48},
 { 54, 0, 107, 48 },
 { 108, 0, 161, 48 },
 { 162, 0, 220, 48 },
};
/*
    Basic player projectile.
*/
class PlayerProjectile1 : public iEntity
{
public:

    PlayerProjectile1();
    ~PlayerProjectile1()
    { }

    void Update(float dTime) override;

private:

    void Init() override;
};

class PlayerProjectile2 : public iEntity
{
public:

    PlayerProjectile2();
    ~PlayerProjectile2()
    { }

    void Update(float dTime) override;

private:

    void Init() override;
};