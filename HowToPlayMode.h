#pragma once
#include "Game.h"            //Game Manager
#include "ModeInterface.h"   //Mode Parent

#include "Sprite.h"          //For making sprite vector arrays
/*
    Simple HowToPlay state to describe the state of play for the player.
*/
class HowToPlayMode : public iMode
{
public:
    //Handle for changing the modes
    inline static const std::string MODE_NAME = "HOW_TO_PLAY";

    HowToPlayMode();
    ~HowToPlayMode();

    //Parent overrides
    void Enter() override;

    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ProcessKey(char key) override;
    std::string GetModeName() const override { return MODE_NAME; }

private:

    //Sprite/object containers
    std::vector<Sprite> backgrounds_;

    SpriteFont* font_;  //Algerian

    //Initialise block
    void InitBackground();

    void RenderText(DirectX::SpriteBatch& batch);

    void Release();
};