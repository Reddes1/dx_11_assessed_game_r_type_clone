#pragma once

/*
    Validate individual character is a-Z in ASCII, or the symbol " - ".
*/
bool ValidateCharAz(char letter);

/*
    Get a random number from given range.
*/
int GetRandNumFromRange(int range);
