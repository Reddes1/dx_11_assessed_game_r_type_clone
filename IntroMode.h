#pragma once

#include "Game.h"            //Game Manager
#include "ModeInterface.h"   //Mode Parent
#include "Sprite.h"          //For making sprite vector arrays
#include "SpriteFont.h"      //For screen text


/*
    Intro screen for the game, currently tooled to provide just a press start prompt. Playing a cutscene, 
    loading data important data could be done here. Bridges into MainMenuMode
*/
class IntroMode : public iMode
{
public:
    //HMode Handle
    inline static const std::string MODE_NAME = "INTRO";

    IntroMode();
    ~IntroMode();

    //Parent overrides
    void Enter() override;
    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ProcessKey(char key) override;
    std::string GetModeName() const override { return MODE_NAME; }


private:
    std::vector<Sprite> backgrounds_;

    //Fonts
    DirectX::SpriteFont* font1_;    //Algerian

    //Initialise block
    void InitBackground();
    //Render any screen text
    void RenderText(DirectX::SpriteBatch& batch);
    //Release resources
    void Release();
};