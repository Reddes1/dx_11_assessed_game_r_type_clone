#include "ModeManager.h"

void ModeManager::SwitchMode(const std::string& newMode) {
				int index = 0;
				assert(!modes_.empty());
				while (index < (int)modes_.size() && modes_[index]->GetModeName() != newMode)
								++index;
				assert(index < (int)modes_.size());
				nextModeIndex_ = index;
}

void ModeManager::Update(float dTime) {
				if (nextModeIndex_ != currentModeIndex_)
								if (currentModeIndex_ == -1 || modes_[currentModeIndex_]->Exit())
								{
												currentModeIndex_ = nextModeIndex_;
												modes_[currentModeIndex_]->Enter();
								}
				modes_[currentModeIndex_]->Update(dTime);
}

void ModeManager::Render(float dTime, DirectX::SpriteBatch& batch) {
				if (currentModeIndex_ >= 0 && currentModeIndex_ < (int)modes_.size())
								modes_[currentModeIndex_]->Render(dTime, batch);
}

void ModeManager::ProcessKey(char key) {
				if (currentModeIndex_ >= 0 && currentModeIndex_ < (int)modes_.size())
								modes_[currentModeIndex_]->ProcessKey(key);
}

void ModeManager::AddMode(iMode* p) {
				assert(p);
				modes_.push_back(p);
}

void ModeManager::Release() {
				for (size_t i = 0; i < modes_.size(); ++i)
								delete modes_[i];
				modes_.clear();
}