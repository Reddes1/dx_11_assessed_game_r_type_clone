#pragma once
#include "EntityInterface.h"        //Object Parent
#include "Sprite.h"                 //For sprites

//Menu frames
const RECTF menuFrames[]
{
    {2, 2, 251, 121},       //Play
    {2, 127, 251, 246},     //Play Highlighted
    {258, 2, 507, 121},     //Option
    {258, 127, 507, 246},   //Option Highlighted
    {2, 252, 251, 371},     //Exit
    {2, 377, 251, 496},     //Exit Highlighted 
};
/*
    Main menu interaction class. Acts as a parent container with scalable vertial alignment for child buttons.
    Structured to handle its own inputs.
*/
class MainMenuObj : public iEntity
{
public:
    MainMenuObj();
    ~MainMenuObj();

    void Init() override;
    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;

private:    

    std::vector<Sprite> menuSprites_;
    
    //Specifically handle GP inputs here.
    void HandleGPInputs();
};