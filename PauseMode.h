#pragma once

#include "Game.h"            //Game Manager
#include "ModeInterface.h"   //Mode Parent
#include "Sprite.h"          //For making sprite vector arrays
#include "SpriteFont.h"      //For screen text


/*
Takes information from the score manager and displays it.
*/
class PauseMode : public iMode
{
public:
    //HMode Handle
    inline static const std::string MODE_NAME = "PAUSE";

    PauseMode();
    ~PauseMode();

    //Parent overrides
    void Enter() override;
    bool Exit() override;
    void Update(float dTime) override;

    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ProcessKey(char key) override;
    std::string GetModeName() const override { return MODE_NAME; }


private:

    //Sprite container
    std::vector<Sprite> backgrounds_;

    //Fonts
    DirectX::SpriteFont* font1_;    //Algerian
                                    
    //For checking how we leave the mode
    GenericFlags8 flags_;

    //Initialise block
    void InitBackground();
    //Render any screen text
    void RenderText(DirectX::SpriteBatch& batch);
    //Process moving to other modes properly
    void ProcessMoveToMainMenu();
    void ProcessMoveToGame();
    //Release resources
    void Release();
};