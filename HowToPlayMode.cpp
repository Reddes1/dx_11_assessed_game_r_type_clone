#include "HowToPlayMode.h"      //Owner

#include "GameConstants.h"      //consts namespaces

HowToPlayMode::HowToPlayMode()
{
    //Reserve Space
    backgrounds_.reserve(2);

    //Load Font
    font_ = new SpriteFont(&Game::Get().GetD3D().GetDevice(), L"data/fonts/algerian.spritefont");

    //Inits
    InitBackground();
}

HowToPlayMode::~HowToPlayMode()
{
    //Clear containers
    backgrounds_.clear();
}

void HowToPlayMode::Enter()
{
    //Update our game state
    Game::Get().SetGameState(GameState::MAIN_MENU_SCREEN);
}

void HowToPlayMode::Release()
{
    backgrounds_.clear();

    delete font_;
    font_ = nullptr;
}

void HowToPlayMode::Update(float dTime)
{
    if (Game::Get().GetGP().IsPressed(0, XINPUT_GAMEPAD_B))
        Game::Get().GetModeManager().SwitchMode("MAIN_MENU");
}

void HowToPlayMode::Render(float dTime, DirectX::SpriteBatch& batch)
{
    //Render background
    for (auto& a : backgrounds_)
    {
        a.Draw(batch);
    }

    RenderText(batch);
}

void HowToPlayMode::ProcessKey(char key)
{
    switch (key)
    {
    case VK_ESCAPE:
        Game::Get().GetModeManager().SwitchMode("MAIN_MENU");
        break;
    }
}

void HowToPlayMode::InitBackground()
{
    assert(backgrounds_.empty());
    //Handy Handle
    Game& game = Game::Get();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Insert sprite and load texture
    backgrounds_.insert(backgrounds_.begin(), FC::HTP_BG_COUNT, Sprite(game.GetD3D()));
    ID3D11ShaderResourceView* p = Game::Get().GetD3D().GetCache().LoadTexture(&game.GetD3D().GetDevice()
        , "data/backgrounds/backgroundatlas1.dds", "Background Atlas 1", false);
    
    //Configure sprite
    for (int i(0); i < FC::HTP_BG_COUNT; ++i)
    {
        backgrounds_[i].SetTexture(*p);
        backgrounds_[i].SetScale(Vector2(width / (FD::backgroundAtlasFrames[1].right - FD::backgroundAtlasFrames[1].left),
            height / (FD::backgroundAtlasFrames[1].bottom - FD::backgroundAtlasFrames[1].top)));
    }
    //Set Texture Rect for each background
    backgrounds_[0].SetTextureRect(FD::backgroundAtlasFrames[2]);
    backgrounds_[1].SetTextureRect(FD::backgroundAtlasFrames[5]);

}

void HowToPlayMode::RenderText(DirectX::SpriteBatch& batch)
{
    //Handy Handles
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //String container
    std::string message = "Press ESC/B to go back";

    Vector2 pos{ width * 0.5f, height * 0.85f };
    RECT textDim = font_->MeasureDrawBounds(message.c_str(), Vector2(0, 0));
    //Draw enter prompt
    font_->DrawString(&batch, message.c_str(), pos, Colours::White, 0, Vector2((float)textDim.right * 0.5f, 1));

}
