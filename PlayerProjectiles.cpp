#include "PlayerProjectiles.h"
#include "GameConstants.h"

/*
    Projectile Type 2
*/
PlayerProjectile1::PlayerProjectile1()
    :iEntity(Game::Get().GetD3D())
{
    Init();
}

void PlayerProjectile1::Init()
{
    //Handy Handles
    MyD3D& d3d = Game::Get().GetD3D();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Calulate and capture adjustments for screen size
    adj_.scaleAdj = Vector2(width / FC::DEFAULT_RES.x, height / FC::DEFAULT_RES.y);
    adj_.spdAdj = Vector2(width / FC::DEFAULT_RES.x, height / FC::DEFAULT_RES.y);

    //Load texture and frames
    std::vector<RECTF> frames(FD::projectileAtlas1Frames, FD::projectileAtlas1Frames +
        sizeof(FD::projectileAtlas1Frames) / sizeof(FD::projectileAtlas1Frames[0]));
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(),
        "data/sprites/projectileatlas.dds", "Projectile Atlas 1", false, &frames);

    spr_.SetTexture(*p);
    spr_.SetTextureRect(frames[0]);
    spr_.SetScale(Vector2(adj_.scaleAdj.x * GC::E2_PROJ_SCALE, adj_.scaleAdj.y * GC::E2_PROJ_SCALE));
    spr_.SetOrigin(Vector2((frames[0].right - frames[0].left) * 0.5f, (frames[0].bottom - frames[0].top) * 0.5f));

    //Set the damage value of this object
    damageValue_ = GC::P_PROJ1_DMG;

    //Flag inactive
    SetIsActive(false);
}

void PlayerProjectile1::Update(float dTime)
{
    if (isActive_)
    {
        Vector2 pos = spr_.GetPosition();
        
        //Calculate new sprite position
        pos += spr_.GetVelocity() * (GC::P_PROJ1_SPEED * adj_.spdAdj) * dTime;
        spr_.SetPosition(pos);

        //If projectile is offscreen
        if(spr_.GetPosition().x + spr_.GetScreenSize().x > WinUtil::Get().GetData().clientWidth)
        {
            isActive_ = false;
        }
    }
}

/*
    Projectile Type 2
*/
PlayerProjectile2::PlayerProjectile2()
    :iEntity(Game::Get().GetD3D())
{
    Init();
}

void PlayerProjectile2::Init()
{
    //Handy Handles
    MyD3D& d3d = Game::Get().GetD3D();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Calulate and capture adjustments for screen size
    adj_.scaleAdj = Vector2(width / FC::DEFAULT_RES.x, height / FC::DEFAULT_RES.y);
    adj_.spdAdj = Vector2(width / FC::DEFAULT_RES.x, height / FC::DEFAULT_RES.y);

    //Load texture and frames
    std::vector<RECTF> frames(FD::projectileAtlas1Frames, FD::projectileAtlas1Frames +
        sizeof(FD::projectileAtlas1Frames) / sizeof(FD::projectileAtlas1Frames[0]));
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(),
        "data/sprites/projectileatlas.dds", "Projectile Atlas 1", false, &frames);

    spr_.SetTexture(*p);
    spr_.SetTextureRect(frames[1]);
    spr_.SetScale(Vector2(adj_.scaleAdj.x * GC::P_PROJ2_SCALE, adj_.scaleAdj.y * GC::P_PROJ2_SCALE));
    spr_.SetOrigin(Vector2((frames[1].right - frames[1].left) * 0.5f, (frames[1].bottom - frames[1].top) * 0.5f));

    //Set the damage value of this object
    damageValue_ = GC::P_PROJ2_DMG;

    //Flag inactive
    SetIsActive(false);
}

void PlayerProjectile2::Update(float dTime)
{
    if (isActive_)
    {
        Vector2 pos = spr_.GetPosition();

        //Calculate new sprite position
        pos += spr_.GetVelocity() * (GC::P_PROJ2_SPEED * adj_.spdAdj) * dTime;
        spr_.SetPosition(pos);

        //If projectile is offscreen
        if (spr_.GetPosition().x + spr_.GetScreenSize().x > WinUtil::Get().GetData().clientWidth)
        {
            isActive_ = false;
        }
    }
}