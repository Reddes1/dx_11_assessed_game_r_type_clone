#include "GameOverMode.h"       //Owner

#include "GameConstants.h"      //consts namespaces

GameOverMode::GameOverMode()
{
    //Allocate Memory & Reserve vector space
    font1_ = new SpriteFont(&Game::Get().GetD3D().GetDevice(), L"data/fonts/algerian.spritefont");
    backgrounds_.reserve(2);

    //Inits
    InitBackground();
}

GameOverMode::~GameOverMode()
{
    Release();
}

void GameOverMode::Enter()
{
    //Update our game state
    Game::Get().SetGameState(GameState::GAME_OVER_SCREEN);
    
    //Reset timer for following enters
    gameOverElapsed_ = 0;
}

void GameOverMode::Update(float dTime)
{
    //Handy Handle
    Game& game = Game::Get();
    //Switch mode when time passes or user inputs
    if (game.GetGP().IsConnected(0))
    {
        if ((gameOverElapsed_ > MC::GAME_OVER_DURATION) && (Game::Get().GetMK().IsPressed(VK_SPACE) ||
            Game::Get().GetGP().IsPressed(0, XINPUT_GAMEPAD_A)))
            Game::Get().GetModeManager().SwitchMode("HIGH_SCORES");
    }



    //Update stage timer
    gameOverElapsed_ += dTime;
}

void GameOverMode::Render(float dTime, DirectX::SpriteBatch& batch)
{
    //Render background
    for (auto& a : backgrounds_)
    {
        a.Draw(batch);
    }

    //Render Text
    RenderText(batch);
}

void GameOverMode::ProcessKey(char key)
{
    switch (key)
    {
    case(VK_SPACE):
        Game::Get().GetModeManager().SwitchMode("HIGH_SCORES");
        break;
    }
}

void GameOverMode::InitBackground()
{
    assert(backgrounds_.empty());
    //Handy Handles
    Game& game = Game::Get();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Insert sprite and load texture
    backgrounds_.insert(backgrounds_.begin(), FC::GAME_OVER_BG_COUNT, Sprite(game.GetD3D()));
    ID3D11ShaderResourceView* p = Game::Get().GetD3D().GetCache().LoadTexture(&game.GetD3D().GetDevice()
        , "data/backgrounds/backgroundatlas1.dds", "Background Atlas 1", false);

    //Configure sprite
    backgrounds_[0].SetTexture(*p);
    backgrounds_[0].SetTextureRect(FD::backgroundAtlasFrames[3]);
    backgrounds_[0].SetScale(Vector2(width / (FD::backgroundAtlasFrames[0].right - FD::backgroundAtlasFrames[0].left),
        height / (FD::backgroundAtlasFrames[0].bottom - FD::backgroundAtlasFrames[0].top)));
}

void GameOverMode::RenderText(DirectX::SpriteBatch& batch)
{
    if (gameOverElapsed_ >= MC::GAME_OVER_DURATION)
    {
        //Handy Handles
        WinUtil& win = WinUtil::Get();
        int width = win.GetData().clientWidth;
        int height = win.GetData().clientHeight;

        std::string message = "Press Space/A to Continue!";

        Vector2 pos{ width * 0.5f, height * 0.8f };
        RECT textDim = font1_->MeasureDrawBounds(message.c_str(), Vector2(0, 0));

        font1_->DrawString(&batch, message.c_str(), pos, Colours::White, 0, Vector2((float)textDim.right * 0.5f, 1));
    }
}

void GameOverMode::Release()
{
    backgrounds_.clear();

    delete font1_;
    font1_ = nullptr;
}
