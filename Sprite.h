#pragma once
#include <stdint.h>
#include "DXAssetManager.h"
#include "SpriteBatch.h"
#include "D3D.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;


enum class SprEffects : short unsigned int
{
    NONE = 0, FLIP_HORIZONTAL = 1, FLIP_VERTICAL = 2, FLIP_BOTH = FLIP_HORIZONTAL | FLIP_VERTICAL,
};

class Sprite; //Forward declaration for Animator class
class Animator
{
private:
    enum class AnimState {START, STOP, CURRENT};
    int animStart_ = 0, animEnd_ = 0, animCurrent_ = 0;
    float animSpeed_ = 0, animElapsedTime_ = 0;
    bool loopAnim_ = false;
    bool playAnim_ = false;
    bool reverseAnim_ = false;
    Sprite& spr_;

public:
    Animator(Sprite& spr)
        :spr_(spr)
    {}

    //Start = FrameID at 0, Stop = Last frame, Speed = animation rate, True = Loop
    void Initialise(int start, int stop, float speed, bool loop, bool reverseAnim = false);
    // Update animation frame
    void Update(float elapsedSec);
    //Start and Stop
    void Play(bool play) { playAnim_ = play; }
    //Set the the animation to play in reverse (TRUE = Reverse, FALSE = Forward) 
    void SetAnimationReverse(bool isReversed);
    //Get the current start frame
    const int GetStartFrame() const { return animStart_; }
    //Get the current ending frame
    const int GetEndFrame() const { return animEnd_; }
    //Get the current active frame
    const int GetCurrentFrame() const { return animCurrent_; }
    //Set new starting frame
    void SetStartFrame(int newStartFrame) { animStart_ = newStartFrame; }
    //Set new ending frame
    void SetEndFrame(int newEndFrame) { animEnd_ = newEndFrame; }
    //Set a new frame manually
    void SetFrame(int frameID);
    void SetAnimationSpeed(float newAnimSpeed) { animSpeed_ = newAnimSpeed; }
    const float GetAnimationSpeed() const { return animSpeed_; }

    Animator& operator=(const Animator& rSide);

};


/*
    Self contained sprite object for use with DX11. Multiple accessor and mutator functions to alter
    a sprite as you might expect, complete with animatior component.
*/
class Sprite
{
public:
    //Init all data objects and pass references
    Sprite(MyD3D& d3d)
        :d3d_(d3d), texData_(nullptr), tex_(nullptr), texRect_{ 0, 0, 0, 0 },
        pos_(0, 0), scale_(1, 1), origin_(0, 0), rot_(0), screenSize_(0, 0),
        colour_(1, 1, 1, 1), depth_(1), spriteEffectID_(SprEffects::NONE),
        vel_(0, 0), animation_(*this)
    {}
    //Need to pass references this way
    Sprite(const Sprite& rSide)
        :d3d_(rSide.d3d_), animation_(*this)
    {
        *this = rSide;
    }
    ~Sprite()
    {}   

    void Draw(SpriteBatch& batch);

    void SetTexture(ID3D11ShaderResourceView& tex, const RECTF& texRect = RECTF{ 0, 0, 0, 0 });
    void SetTextureRect(const RECTF& texRect);
    void SetPosition(Vector2& pos);
    void SetVelocity(const Vector2& vel);
    void SetScale(const Vector2& scale);
    void SetOrigin(const Vector2& origin);
    void SetColour(const Vector4& colour);
    void SetRotation(const float rot);
    void SetDepth(const float depth);
    void SetSpriteEffect(SprEffects effects);
    void SetFrame(int frameID);
     
    void ScrollTexture(float x, float y);

    Vector2 GetPosition() const;
    Vector2 GetVelocity() const;
    Vector2 GetScale() const;
    Vector2 GetOrigin() const;
    float GetRotation() const;
    float GetDepth() const;
    Vector4 GetColour() const;
    Vector2 GetTextureRect() const;
    Vector2 GetScreenSize() const;

    Vector4 GetCollisionValues() const;


    Animator& GetAnimation()
    {
        return animation_;
    }
    MyD3D& GetD3D()
    {
        return d3d_;
    }
    const DXAssetManager::Data& GetTextureData() const
    {
        assert(texData_);
        return *texData_;
    }
    ID3D11ShaderResourceView& GetTexture()
    {
        assert(tex_);
        return *tex_;
    }

    Sprite& operator=(const Sprite& rhs);

private:
    //Data
    Animator animation_;
    RECTF texRect_;
    Vector4 colour_;
    Vector2 pos_;
    Vector2 vel_;
    Vector2 scale_;
    Vector2 origin_;
    Vector2 screenSize_;
    ID3D11ShaderResourceView* tex_;
    MyD3D& d3d_;
    const DXAssetManager::Data* texData_;
    float rot_;
    float depth_;
    SprEffects spriteEffectID_;

    void UpdateScreenSize();    //Called during SetScale, ensures when screenSize is called its up-to-date.

};