#pragma once

#include "EntityInterface.h"    //Object Parent
#include "Sprite.h"             //Sprite class
#include "StageOneMode.h"       //Mode Owner
#include "FlagSets.h"           //Object state flags



/*
    Basic enemy with no special attributes
*/

class Enemy1 : public iEntity
{
public:

    Enemy1();
    ~Enemy1()
    {}

    //Parent overrides
    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ResetVariables() override;

    //Set parent mode
    void SetMode(StageOneMode& mode) { ownerMode_ = &mode; }

private:

    //What mode does this sprite belong to
    StageOneMode* ownerMode_ = nullptr;

    //Flags
    ObjectStateFlags flags1_;

    //Initialise block
    void Init() override;

};