#pragma once
/*
    One-stop shop for collision methods between generated hitboxes. Has 3 collision methods to try fit the best
    scenario for expected object collision. Best used with centered origins for objects (pending improvements).
*/
class CollisionManager
{
public:
    CollisionManager()
    {}
    ~CollisionManager()
    {}

    //Rect/Rect collision, Takes Vector4 (posX, posY, Width, Height). Assumes position is centered origin.
    bool Rect2RectCollision(Vector4 lRect, Vector4 rRect)
    {
        //Alter the rect positions for calculations (This moves them from origin @ center to Left/Top)
        lRect.x -= lRect.z * 0.5f;
        lRect.y -= lRect.w * 0.5f;

        rRect.x -= rRect.z * 0.5f;
        rRect.y -= rRect.w * 0.5f;

        //Check collision
        if (lRect.x + lRect.z >= rRect.x &&     //Rect1 Right edge past Rect2 Left
            lRect.x <= rRect.x + rRect.z &&     //Rect1 Left edge past Rect2 right
            lRect.y + lRect.w >= rRect.y &&     //Rect1 Top edge past Rect2 bottom
            lRect.y <= rRect.y + rRect.w)       //Rect1 bottom edge past Rect2 top
            return true;
        return false;
    }

    //Rect/Rect collision, Takes Vector4 (posX, posY, Width, Height). Assumes position is centered origin. Takes scaling arguments.
    bool Rect2RectCollision(Vector4 lRect, Vector4 rRect, float lRectXScale, float lRectYScale, float rRectXScale, float rRectYScale)
    {
        lRect.z *= lRectXScale;
        lRect.w *= lRectYScale;
        rRect.z *= rRectXScale;
        rRect.w *= rRectYScale;

        return Rect2RectCollision(lRect, rRect);
    }
    
    //Cir/Cir Collision, Takes Vector4 (posX, posY, Width, Height) Assumes position is centered origin.
    bool Cir2CirCollision(const Vector4 lObject, const Vector4 rObject)
    {

        float radiusL = (Vector2(lObject.z, lObject.w).Length() * 0.5f);  //Adjust to make the hitbox a little more generous(Move to interface later)
        float radiusR = (Vector2(rObject.z, rObject.w).Length() * 0.5f);

        float distance = Vector2{ lObject.x - rObject.x, lObject.y - rObject.y }.Length();
        
        //Check collision
        if (distance <= radiusL + radiusR)
            return true;
        return false;
    }

    //Cir/Cir Collision, Takes Vector4 (posX, posY, Width, Height) Assumes position is centered origin. Takes scaling arguments.
    bool Cir2CirCollision(Vector4 lObject, Vector4 rObject, float lObjScale, float rObjScale)
    {
        //Rescale left circle
        lObject.z *= lObjScale;
        lObject.w *= lObjScale;
        //Rescale right circle
        rObject.z *= rObjScale;
        rObject.w *= rObjScale;

        //Pass new values through collision function and return
        return (Cir2CirCollision(lObject, rObject));
    }

    //Cir/Rect Collision, Takes Vector4 (posX, posY, Width, Height). Assumes position is centered origin.
    bool Cir2RectCollision(Vector4 lObjectCir, Vector4 rObjectRect)
    {
        //Alter the rect position for calculations (This moves it from origin @ center to Left/Top)
        rObjectRect.x -= rObjectRect.z * 0.5f;
        rObjectRect.y -= rObjectRect.w * 0.5f;
        //Get raidus of cicle obj & make the hitbox a little generous (Move this into an interface option later).
        float radius = (Vector2{ lObjectCir.z, lObjectCir.w }.Length() / 2.f) * 0.7f;    
        
        //Comparion containers
        float compX = lObjectCir.x;
        float compY = lObjectCir.y;
        
        //Check what edges we are closest to
        if (lObjectCir.x < rObjectRect.x)                 //Left Edge
            compX = rObjectRect.x;
        else if (lObjectCir.x > rObjectRect.x + rObjectRect.z)   //Right Edge
            compX = rObjectRect.x + rObjectRect.z;
        if (lObjectCir.y < rObjectRect.y)                 //Top Edge
            compY = rObjectRect.y;
        else if (lObjectCir.y > rObjectRect.y + rObjectRect.w)   //Bottom Edge
            compY = rObjectRect.y + rObjectRect.w;

        //Calculate distance
        float distance = Vector2{ lObjectCir.x - compX, lObjectCir.y - compY }.Length();

        //Check collision
        if (distance <= radius)
            return true;
        return false;
    }

    //Cir/Rect Collision, Takes Vector4 (posX, posY, Width, Height). Assumes position is centered origin. Takes scaling arguments.
    bool Cir2RectCollision(Vector4 lObjectCir, Vector4 rObjectRect, float lCirScaling, float rRectScaleX, float rRectScaleY)
    {
        //Rescale circle
        lObjectCir.z *= lCirScaling;
        lObjectCir.w *= lCirScaling;
        //Rescale rect
        rObjectRect.z *= rRectScaleX;
        rObjectRect.w *= rRectScaleY;

        //Pass new values through collision function and return
        return Cir2RectCollision(lObjectCir, rObjectRect);
    }

private:

};