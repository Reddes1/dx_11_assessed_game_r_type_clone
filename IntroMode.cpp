#include "IntroMode.h"          //Owner

#include "GameConstants.h"      //consts namespaces

IntroMode::IntroMode()
{
    //Allocate Memory & Reserve vector space
    font1_ = new SpriteFont(&Game::Get().GetD3D().GetDevice(), L"data/fonts/algerian.spritefont");
    backgrounds_.reserve(2);

    //Inits
    InitBackground();
}

IntroMode::~IntroMode()
{
    Release();
}

void IntroMode::Enter()
{
    //Update out game state
    Game::Get().SetGameState(GameState::INTRO_SCREEN);
}

void IntroMode::Release()
{
    backgrounds_.clear();

    delete font1_;
    font1_ = nullptr;
}

void IntroMode::Update(float dTime)
{
    if(Game::Get().GetGP().IsPressed(0, XINPUT_GAMEPAD_START))
        Game::Get().GetModeManager().SwitchMode("MAIN_MENU");

}

void IntroMode::Render(float dTime, DirectX::SpriteBatch& batch)
{
    for (auto& b : backgrounds_)
    {
        b.Draw(batch);
    }

    RenderText(batch);
}

void IntroMode::ProcessKey(char key)
{
    switch (key)
    {
    case VK_ESCAPE:
    case VK_SPACE:
        Game::Get().GetModeManager().SwitchMode("MAIN_MENU");
        break;
    }
}

void IntroMode::InitBackground()
{
    assert(backgrounds_.empty());
    //Handy Handles
    Game& game = Game::Get();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Insert sprite and load texture
    backgrounds_.insert(backgrounds_.begin(), FC::INTRO_BG_COUNT, Sprite(game.GetD3D()));
    ID3D11ShaderResourceView* p = Game::Get().GetD3D().GetCache().LoadTexture(&game.GetD3D().GetDevice()
        , "data/backgrounds/backgroundatlas1.dds", "Background Atlas 1", false);
   
    //Configure sprite
    backgrounds_[0].SetTexture(*p);
    backgrounds_[0].SetTextureRect(FD::backgroundAtlasFrames[0]);
    backgrounds_[0].SetScale(Vector2(width / (FD::backgroundAtlasFrames[0].right - FD::backgroundAtlasFrames[0].left),
        height / (FD::backgroundAtlasFrames[0].bottom - FD::backgroundAtlasFrames[0].top )));
}

void IntroMode::RenderText(DirectX::SpriteBatch& batch)
{
    //Handy Handles
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    std::string message = "Press Space/Start to Start!";

    Vector2 pos{ width * 0.5f, height * 0.8f };
    RECT textDim = font1_->MeasureDrawBounds(message.c_str(), Vector2(0, 0));

    font1_->DrawString(&batch, message.c_str(), pos, Colours::White, 0, Vector2((float)textDim.right * 0.5f, 1));
}
