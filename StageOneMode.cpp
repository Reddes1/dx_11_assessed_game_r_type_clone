#include "StageOneMode.h"       //Owner
#include "GameConstants.h"      //consts namespaces
#include "D3DUtil.h"            //DX specific utils
#include <algorithm>            //std::sort

//Level Objects
#include "Player.h"
#include "Enemy1.h"
#include "Enemy2.h"
#include "PlayerProjectiles.h"
#include "EnemyProjectile1.h"
#include "PowerUp.h"

StageOneMode::StageOneMode()
{
    Init();
}
StageOneMode::~StageOneMode()
{
    Release();
}

void StageOneMode::Enter()
{
    //Update Game State
    Game::Get().SetGameState(GameState::STAGE_ONE);

    //Check global flag(s) for Enter rules
    //Flag 1 = Re-entering from menu after pause
    if (Game::Get().GetGlobalFlags().flag1)
    {
        modeStates_.isNewGame = true;
        modeStates_.isPaused = false;
    }

    //If pause is true, then its not a new game so change flags
    if (modeStates_.isPaused)
    {
        modeStates_.isPaused = false;
    }
    //If entering from post gameover or fresh, and checking that its not from a pause
    else if ((modeStates_.isNewGame || modeStates_.isGameOver) && !modeStates_.isPaused)
    {
        //Clear the screen
        MakeAllInactive();

        //Stop current track
        AudioMgrFMOD::Get().GetSongMgr()->Stop();

        //Build Level
        Init();

        //Set flags to appropriate states
        modeStates_.isGameOver = false;
        modeStates_.isNewGame = false;
        Game::Get().GetGlobalFlags().flag1 = false;
    }

    //Configure mouse
    Game::Get().GetMK().SetMouseStatus(false, true);

}
bool StageOneMode::Exit()
{
    //Stop audio if leaving via gameover
    if(modeStates_.isGameOver)
        AudioMgrFMOD::Get().GetSongMgr()->Stop();

    //Configure mouse
    Game::Get().GetMK().SetMouseStatus(true, false);
    return true;
}

void StageOneMode::Release()
{
    //Delete all objects in container
    for (size_t i(0); i < gameObjects_.size(); ++i)
    {
        delete gameObjects_[i];
    }

    //Delete all objects in container
    for (size_t i(0); i < projObjects_.size(); ++i)
    {
        delete projObjects_[i];
    }

    //Delete fonts
    delete font1_;
    font1_ = nullptr;

    //Clear containers
    gameObjects_.clear();
    projObjects_.clear();
    backgroundScene_.clear();
}

void StageOneMode::Update(float dTime)
{
    //Process what state we are in and act 
    UpdateStageState();
    UpdateBackground(dTime);

    //Update game object array
    for (auto& a : gameObjects_)
    {
        if (a->GetIsActive())
        {
            a->Update(dTime);
        }
    }
    //Update projectile object array
    for (auto& a : projObjects_)
    {
        if (a->GetIsActive())
        {
            a->Update(dTime);
        }
    }

    //Check entity collision
    ManageCollisions();
    //Update Gamepad inputs
    UpdateGPInputs();
    //Update timers
    UpdateStageTimers(dTime);
}

void StageOneMode::Render(float dTime, DirectX::SpriteBatch& batch)
{
    //Render scene
    for (auto& a : backgroundScene_)
    {
        a.Draw(batch);
    }

    //Render projectiles
    for (auto& a : projObjects_)
    {
        if (a->GetIsActive())
        {
            a->Render(dTime, batch);
        }
    }

    //Render game objects
    for (auto& a : gameObjects_)
    {
        if (a->GetIsActive())
        {
            a->Render(dTime, batch);
        }
    }

    //Render screen text
    RenderText(batch);
}

void StageOneMode::RenderText(DirectX::SpriteBatch& batch)
{
    //Handy Handles
    Game& game = Game::Get();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Construct score string
    std::stringstream msg;
    msg << "Score: " << std::setw(5) << std::setfill('0') << std::to_string(game.GetScoreManager().GetActivePlayerScore());

    //Get a position and the texture dimensions
    Vector2 pos{ width * 0.08f, height * 0.95f };
    RECT textDim = font1_->MeasureDrawBounds(msg.str().c_str(), Vector2(0, 0));

    //Draw our string
    font1_->DrawString(&batch, msg.str().c_str(), pos, Colours::White, 0, Vector2((float)textDim.right * 0.5f, 1));
}

void StageOneMode::UpdateGPInputs()
{
    //Handy Handle
    Game& game = Game::Get();

    if(game.GetGP().IsConnected(0))
        if(game.GetGP().IsPressed(0, XINPUT_GAMEPAD_START))
            ProcessPause();
}

void StageOneMode::ProcessGameOver()
{
    //Flag for gameover
    modeStates_.isGameOver = true;

    Game::Get().GetModeManager().SwitchMode("GAME_OVER");
}

void StageOneMode::ProcessPause()
{
    //Flag for pause
    modeStates_.isPaused = true;
    Game::Get().GetModeManager().SwitchMode("PAUSE");
}

void StageOneMode::ProcessKey(char key)
{
    switch (key)
    {
    case VK_P:
    case ('p'):
        //Call Pause Here
        ProcessPause();
        break;
    }
}

void StageOneMode::UpdateStageState()
{
    /*
        Determine what state we should be in by the current level time, and spawn a power-up container on the switch.
    */
    if (timers_.stageElapsed_ >= GC::STAGE1_TIME_GATE5)
    {
        currentState_ = STAGE_STATES::STATE_6;
    }
    if (timers_.stageElapsed_ >= GC::STAGE1_TIME_GATE4)
    {
        currentState_ = STAGE_STATES::STATE_5;
    }
    else if (timers_.stageElapsed_ >= GC::STAGE1_TIME_GATE3)
    {
        currentState_ = STAGE_STATES::STATE_4;
    }
    else if (timers_.stageElapsed_ >= GC::STAGE1_TIME_GATE2)
    {
        currentState_ = STAGE_STATES::STATE_3;
    }
    else if (timers_.stageElapsed_ >= GC::STAGE1_TIME_GATE1)
    {
        currentState_ = STAGE_STATES::STATE_2;
    }

    //Independant of current state, spawn a powerup container when the timer allows
    if ((timers_.stageSpawnerElapsed_ - timers_.puSpawnElapsed_) > GC::PU_SPAWN_DELAY)
    {
        SpawnEnemy(typeid(PowerUp), GetRandom(0.4f, 0.8f), 1);
        timers_.puSpawnElapsed_ = timers_.stageSpawnerElapsed_;

    }

    //Using our current state, decide what we should be spawning, how many and how frequently.
    switch (currentState_)
    {
    case(STAGE_STATES::STATE_1):
        if ((timers_.stageSpawnerElapsed_ - timers_.e1SpawnElapsed_) > GC::E1_SPAWN_DELAY1)
        {
            SpawnEnemy(typeid(Enemy1), GetRandom(0.4f, 0.8f), 3);
            timers_.e1SpawnElapsed_ = timers_.stageSpawnerElapsed_;
        }
        if ((timers_.stageSpawnerElapsed_ - timers_.e2SpawnElapsed_) > GC::E2_SPAWN_DELAY1)
        {
            SpawnEnemy(typeid(Enemy2), GetRandom(0.2f, 0.4f), 1);
            timers_.e2SpawnElapsed_ = timers_.stageSpawnerElapsed_;
        }
        break;
    case(STAGE_STATES::STATE_2):
        if ((timers_.stageSpawnerElapsed_ - timers_.e1SpawnElapsed_) > GC::E1_SPAWN_DELAY2)
        {
            SpawnEnemy(typeid(Enemy1), GetRandom(0.2f, 0.6f), GetRandom(3, 4));
            timers_.e1SpawnElapsed_ = timers_.stageSpawnerElapsed_;
        }
        if ((timers_.stageSpawnerElapsed_ - timers_.e2SpawnElapsed_) > GC::E2_SPAWN_DELAY2)
        {
            SpawnEnemy(typeid(Enemy2), GetRandom(0.6f, 0.8f), 2);
            timers_.e2SpawnElapsed_ = timers_.stageSpawnerElapsed_;
        }
        break;
    case(STAGE_STATES::STATE_3):
        if ((timers_.stageSpawnerElapsed_ - timers_.e1SpawnElapsed_) > GC::E1_SPAWN_DELAY2)
        {
            SpawnEnemy(typeid(Enemy1), GetRandom(0.3f, 0.8f), GetRandom(4, 5));
            timers_.e1SpawnElapsed_ = timers_.stageSpawnerElapsed_;
        }
        if ((timers_.stageSpawnerElapsed_ - timers_.e2SpawnElapsed_) > GC::E2_SPAWN_DELAY1)
        {
            SpawnEnemy(typeid(Enemy2), GetRandom(0.2f, 0.2f), 3);
            timers_.e2SpawnElapsed_ = timers_.stageSpawnerElapsed_;
        }
        break;
    case(STAGE_STATES::STATE_4):
        if ((timers_.stageSpawnerElapsed_ - timers_.e2SpawnElapsed_) > GC::E2_SPAWN_DELAY1)
        {
            SpawnEnemy(typeid(Enemy2), 0.15f, 3);
            SpawnEnemy(typeid(Enemy2), 0.85f, 3);
            timers_.e2SpawnElapsed_ = timers_.stageSpawnerElapsed_;
        }
        //Hijack the E1 timer to spawn more E2 types with a different pattern
        if ((timers_.stageSpawnerElapsed_ - timers_.e1SpawnElapsed_) > GC::E2_SPAWN_DELAY3)
        {
            SpawnEnemy(typeid(Enemy2), 0.5f, 2);
            timers_.e1SpawnElapsed_ = timers_.stageSpawnerElapsed_;
        }
        break;
    case(STAGE_STATES::STATE_5):
        if ((timers_.stageSpawnerElapsed_ - timers_.e2SpawnElapsed_) > GC::E2_SPAWN_DELAY4)
        {
            SpawnEnemy(typeid(Enemy2), GetRandom(0.2f, 0.8f), 1);
            timers_.e2SpawnElapsed_ = timers_.stageSpawnerElapsed_;
        }
        break;
    case(STAGE_STATES::STATE_6):
        if ((timers_.stageSpawnerElapsed_ - timers_.e2SpawnElapsed_) > GC::E2_SPAWN_DELAY4)
        {
            SpawnEnemy(typeid(Enemy2), GetRandom(0.2f, 0.8f), 2);
            timers_.e2SpawnElapsed_ = timers_.stageSpawnerElapsed_;
        }
    }
}

void StageOneMode::ManageCollisions()
{
    //Grab a hold of our player object
    iEntity* p = FindFirst(gameObjects_, typeid(Player), true);

    PlayerCollisions(p);
    EnemyCollisions(p);
}

void StageOneMode::EnemyCollisions(iEntity* p)
{
    //Handy Handle
    Game& game = Game::Get();

    //Enemy Objects/Player Object 
    if (p)
    {
        for (size_t i(0); i < gameObjects_.size(); ++i)
        {
            //If the player object has collided with any other game entity that isn't itself, other than positive collisions
            if (gameObjects_[i]->GetIsActive() && gameObjects_[i] != p && !dynamic_cast<PowerUp*>(gameObjects_[i]) &&
                game.GetCollisionManager().Rect2RectCollision(gameObjects_[i]->GetCollisionValues(), p->GetCollisionValues()))
            {
                ProcessGameOver();
                //Early out, save fractional time
                i = gameObjects_.size();
            }
        }
    }


    //Enemy Projectile/Player Object
    if (p)
    {
        for (size_t i(0); i < projObjects_.size(); ++i)
        {
            //Proj is active, same type as EProj1 & collision is true
            if (projObjects_[i]->GetIsActive() && typeid(*projObjects_[i]) == typeid(EnemyProjectile1) &&
                game.GetCollisionManager().Cir2RectCollision(projObjects_[i]->GetCollisionValues(), p->GetCollisionValues()))
            {
                ProcessGameOver();
            }
        }
    }
}

void StageOneMode::ProcessUpgradeEffect(iEntity* p, iEntity* pUp)
{
    //Play audio
    AudioMgrFMOD::Get().GetSfxMgr()->Play("PowerUpSFX", false, false, nullptr, 0.05f);
    //Cast as player as we need access to interfaces
    Player* pl = dynamic_cast<Player*>(p);
    PowerUp* powerUp = dynamic_cast<PowerUp*>(pUp);
    switch ((int)powerUp->GetPowerUpType())
    {
    case(0):
        pl->UpgradeShip('s');
        break;
    case(1):
        pl->UpgradeShip('w');
        break;
    }
    //Make powerup inactive
    powerUp->SetIsActive(false);
}
void StageOneMode::PlayerCollisions(iEntity* p)
{
    //Handy Handle
    Game& game = Game::Get();

    //Player Object/Powerup Object
    if (p)
    {
        for (size_t i(0); i < gameObjects_.size(); ++i)
        {
            //Object is Active, not the player, Same type as PowerUp & collision is true
            if (gameObjects_[i]->GetIsActive() && gameObjects_[i] != p && typeid(*gameObjects_[i]) == typeid(PowerUp) &&
                game.GetCollisionManager().Cir2RectCollision(gameObjects_[i]->GetCollisionValues(), p->GetCollisionValues()))
            {
                //Recast the pointer for interface access
                PowerUp* powerUp = dynamic_cast<PowerUp*>(gameObjects_[i]);
                //If the PowerUp is still a container, then the player dies like any other enemy collision
                if ((int)powerUp->GetCurrentState() == 0)
                    ProcessGameOver();
                else
                {
                    //Process what upgrade the player
                    ProcessUpgradeEffect(p, gameObjects_[i]);
                    //Unlikely to be more than one on screen, so early out.
                    i = gameObjects_.size();
                }
            }
        }
    }

    //Player Projectile/Power-Up Object
    for (size_t i(0); i < projObjects_.size(); ++i)
    {
        //If the projectile is active and owned by the player by type
        if (projObjects_[i]->GetIsActive() && (typeid(*projObjects_[i]) == typeid(PlayerProjectile1) || typeid(*projObjects_[i]) == typeid(PlayerProjectile2)))
        {
            //Got a proj, go through the objects
            for (size_t j(0); j < gameObjects_.size(); ++j)
            {
                //Object is active, not the player, same type as Powerup & collision is true
                if (gameObjects_[j]->GetIsActive() && gameObjects_[j] != p && (typeid(*gameObjects_[j]) == typeid(PowerUp)) &&
                    game.GetCollisionManager().Cir2RectCollision(gameObjects_[j]->GetCollisionValues(), projObjects_[i]->GetCollisionValues()))
                {
                    //Recast the pointer as a PowerUp class
                    PowerUp* powerUp = dynamic_cast<PowerUp*>(gameObjects_[j]);
                    //If a container, the projectile will collide as expected
                    if ((int)powerUp->GetCurrentState() == 0)
                    {
                        //Deactivate projectile
                        projObjects_[i]->SetIsActive(false);
                        //Reduce the HP value based on the damage value of the projectile
                        gameObjects_[j]->ReduceHPValue(projObjects_[i]->GetDamageValue());
                        //Early out, the projectile doesn't exist anymore
                        j = gameObjects_.size();
                    }
                }
            }
        }
    }

    //Player Projectile/Enemy Object
    for (size_t i(0); i < projObjects_.size(); ++i)
    {
        //Proj is active, same type as PProj1/2
        if (projObjects_[i]->GetIsActive() && (typeid(*projObjects_[i]) == typeid(PlayerProjectile1) || typeid(*projObjects_[i]) == typeid(PlayerProjectile2)))
        {
            //We found our projectile, now go through the gameobjects array
            for (size_t j(0); j < gameObjects_.size(); ++j)
            {
                //If our object is active, not the player and not a power up (handled else where)
                if (gameObjects_[j]->GetIsActive() && gameObjects_[j] != p && (typeid(*gameObjects_[j]) != typeid(PowerUp)) &&
                    game.GetCollisionManager().Rect2RectCollision(gameObjects_[j]->GetCollisionValues(), projObjects_[i]->GetCollisionValues(), 1, 1, 0.8f, 0.8f))
                {
                    //Deactivate projectile
                    projObjects_[i]->SetIsActive(false);
                    //Reduce the HP value based on the damage value of the projectile
                    gameObjects_[j]->ReduceHPValue(projObjects_[i]->GetDamageValue());
                    //Early out, the projectile doesn't exist anymore
                    j = gameObjects_.size();

                }
            }
        }
    }
}

void StageOneMode::SpawnEnemy(const std::type_info& enemyType, float yPos, float spawnCount)
{
    //Handy Handles
    Game& game = Game::Get();
    WinUtil& winUtil = WinUtil::Get();

    int width = winUtil.GetData().clientWidth;
    int height = winUtil.GetData().clientHeight;

    //Spawn as many as asked for
    for (short int i(0); i < spawnCount; ++i)
    {
        //Find an object of the type asked for
        iEntity* enemy = FindFirst(gameObjects_, enemyType, false);
        //If found
        if (enemy)
        {
            //Generate a position
            float offset = enemy->GetSprite().GetScreenSize().x * 1.4f;
            //Generate a slight offset from where asked 
            Vector2 pos = Vector2(width + offset + (offset * i), (height * yPos) + enemy->GetSprite().GetScreenSize().y
                * GetRandom(-1.f, 1.f));

            //Set position and activate the object
            enemy->GetSprite().SetPosition(pos);
            enemy->ResetVariables();
            enemy->SetIsActive(true);
        }
    }
}

void StageOneMode::MakeAllInactive()
{
    //Make objects inactive 
    for (auto& a : gameObjects_)
        a->SetIsActive(false);
    //Make projectiles inactive
    for (auto& a : projObjects_)
        a->SetIsActive(false);
}

void StageOneMode::UpdateBackground(float dTime)
{
    //Scroll the background independently
    backgroundScene_[0].ScrollTexture(5 * dTime, 0);

    //Move the position of the planets
    for (int i(1); i < backgroundScene_.size(); ++i)
    {
        backgroundScene_[i].SetPosition(Vector2(backgroundScene_[i].GetPosition().x + ((-10 - i) * dTime),
            backgroundScene_[i].GetPosition().y));

        //If planet is now offscreen, wrap it back around
        if (backgroundScene_[i].GetPosition().x + (backgroundScene_[i].GetScreenSize().x * 0.5f) < 0)
        {
            backgroundScene_[i].SetPosition(Vector2(WinUtil::Get().GetData().clientWidth + (backgroundScene_[i].GetScreenSize().x * 0.5f),
                backgroundScene_[i].GetPosition().y));
        }
    }
}

void StageOneMode::UpdateStageTimers(float dTime)
{
    //Update internal stage timer
    timers_.stageElapsed_ += dTime;
    //Update spawn timers
    timers_.stageSpawnerElapsed_ += dTime;
}

void StageOneMode::Init()
{
    //During load up
    if (!modeStates_.isLoaded)
    {
        InitObjectContainers();
        InitBackground();
        //Load font
        font1_ = new SpriteFont(&Game::Get().GetD3D().GetDevice(), L"data/fonts/algerian.spritefont");
        //Flag that loading is done
        modeStates_.isLoaded = true;
    }
    //Configure game objects and variable states
    ConfigureBackground();
    ConfigureGameState();
}

void StageOneMode::ConfigureBackground()
{
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Scale our starfield background seperately relative to the screen
    backgroundScene_[0].SetScale(Vector2(width / (FD::stageOneBackgroundAtlasFrames[0].right - FD::stageOneBackgroundAtlasFrames[0].left),
        height / (FD::stageOneBackgroundAtlasFrames[0].bottom - FD::stageOneBackgroundAtlasFrames[0].top)));

    //Seed random planet setup
    SeedRandom(-1);

    //Configure the background, with a random scaling each time
    for (size_t i(1); i < backgroundScene_.size(); ++i)
    {
        float randScaling = GetRandom(0.05f, 0.5f);
        backgroundScene_[i].SetOrigin(Vector2(backgroundScene_[i].GetTextureRect() * 0.5f));
        backgroundScene_[i].SetScale(Vector2(height / (FD::stageOneBackgroundAtlasFrames[i].right - FD::stageOneBackgroundAtlasFrames[i].left),
            height / (FD::stageOneBackgroundAtlasFrames[i].bottom - FD::stageOneBackgroundAtlasFrames[i].top)) * randScaling);
        backgroundScene_[i].SetPosition(Vector2(GetRandom(0, static_cast<float>(width)), GetRandom(0, static_cast<float>(height * 0.9f))));

    }
    //Sort the array using the X scale and a lambda so that in the case of smaller planets, they don't get hidden by the larger ones
    std::sort(backgroundScene_.begin() + 1, backgroundScene_.end(), [](const auto& lSide, const auto& rSide)
        {
            return static_cast<float>(lSide.GetScale().x) > static_cast<float>(rSide.GetScale().x);
        }
    );
}

void StageOneMode::ConfigureGameState()
{
    //Handy handle
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Reset Timers
    timers_.stageElapsed_ = 0;
    timers_.stageSpawnerElapsed_ = 0;
    timers_.e1SpawnElapsed_ = -2;
    timers_.e2SpawnElapsed_ = -2;
    timers_.puSpawnElapsed_ = 0;

    //Set playarea values
    playArea_.left = 0 + (width * 0.02f);
    playArea_.top = 0 + (height * 0.02f);
    playArea_.right = width - playArea_.left;
    playArea_.bottom = height * 0.90f;

    //Set Stage State
    currentState_ = STAGE_STATES::STATE_1;

    //Find the player object in the array, reset its variables and 
    iEntity* p = FindFirst(gameObjects_, typeid(Player), false);
    if (p)
    {
        p->ResetVariables();
        p->SetIsActive(true);
    }

    //Reset player score
    Game::Get().GetScoreManager().SetActivePlayerScore(0);


}

void StageOneMode::InitBackground()
{
    assert(backgroundScene_.empty());

    //Handy Handles
    Game& game = Game::Get();
    WinUtil& win = WinUtil::Get();

    //Insert sprites and load texture
    backgroundScene_.insert(backgroundScene_.begin(), FC::STAGE_ONE_BG_COUNT, Sprite(game.GetD3D()));
    ID3D11ShaderResourceView* p = Game::Get().GetD3D().GetCache().LoadTexture(&game.GetD3D().GetDevice()
        , "data/backgrounds/stageoneatlas1.dds", "Stage One Atlas", false);

    //Set texture/Rect
    for (size_t i(0); i < backgroundScene_.size(); ++i)
    {
        backgroundScene_[i].SetTexture(*p);
        backgroundScene_[i].SetTextureRect(FD::stageOneBackgroundAtlasFrames[i]);
    }
}

void StageOneMode::InitObjectContainers()
{
    assert(gameObjects_.empty());
    assert(projObjects_.empty());
    //Reserve memory
    gameObjects_.reserve(FC::OBJECT_RESERVE);
    projObjects_.reserve(FC::PROJ_RESERVE);

    //Add Enemy1
    for (int i(0); i < FC::E1_COUNT; ++i)
        Add(gameObjects_, new Enemy1());

    //Add Enemy2, with stage reference
    for (int i(0); i < FC::E2_COUNT; ++i)
    {
        Enemy2* e = new Enemy2();
        e->SetMode(*this);
        Add(gameObjects_, e);
    }
    //Add Powerup, with stage reference
    for (int i(0); i < FC::PU_COUNT; ++i)
    {
        PowerUp* pu = new PowerUp();
        pu->SetMode(*this);
        Add(gameObjects_, pu);
    }

    //Add player, give stage reference
    Player* p = new Player();
    p->SetMode(*this);
    p->SetIsActive(true);
    Add(gameObjects_, p);

    //Add Projectile types
    for (int i(0); i < FC::P_PROJ1_COUNT; ++i)
    {
        Add(projObjects_, new PlayerProjectile1());
    }
    for (int i(0); i < FC::P_PROJ2_COUNT; ++i)
    {
        Add(projObjects_, new PlayerProjectile2());
    }
    for (int i(0); i < FC::E2_PROJ_COUNT; ++i)
    {
        Add(projObjects_, new EnemyProjectile1());
    }
}

void StageOneMode::Add(std::vector<iEntity*>& container, iEntity* object)
{
    assert(object);
    container.push_back(object);
}

void StageOneMode::Remove(std::vector<iEntity*>& container, iEntity* object)
{
    size_t aSize = container.size();
    assert(aSize > 0);
    container.erase(std::remove(container.begin(), container.end(), object));
    assert(aSize != container.size());
    delete object;
}

iEntity* StageOneMode::FindFirstObject(const std::type_info& type, bool active)
{
    size_t i = 0;
    while (i < gameObjects_.size() && (typeid(*gameObjects_[i]) != type || gameObjects_[i]->GetIsActive() != active))
        ++i;
    if (i >= gameObjects_.size())
        return nullptr;
    return gameObjects_[i];
}

iEntity* StageOneMode::FindFirstProjectile(const std::type_info& type, bool active)
{
    size_t i = 0;
    while (i < projObjects_.size() && (typeid(*projObjects_[i]) != type || projObjects_[i]->GetIsActive() != active))
        ++i;
    if (i >= projObjects_.size())
        return nullptr;
    return projObjects_[i];
}

iEntity* StageOneMode::FindFirst(std::vector<iEntity*>& container, const std::type_info& type, bool active)
{
    size_t i = 0;
    while (i < container.size() && (typeid(*container[i]) != type || container[i]->GetIsActive() != active))
        ++i;
    if (i >= container.size())
        return nullptr;
    return container[i];
}

RECTF& StageOneMode::GetPlayArea()
{
    return playArea_;
}
