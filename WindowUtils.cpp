#include "WindowUtils.h"

LRESULT WinUtil::DefaultMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch (msg)
    {
        // WM_ACTIVATE is sent when the window is activated or deactivated.  
        // We pause the game when the window is deactivated and unpause it 
        // when it becomes active.  
    case WM_ACTIVATE:
        if (LOWORD(wParam) == WA_INACTIVE)
        {
            windowData_.appPaused = true;
        }
        else
        {
            windowData_.appPaused = false;
        }
        return 0;

        // WM_SIZE is sent when the user resizes the window.  
    case WM_SIZE:
        // Save the new client area dimensions.
        windowData_.clientWidth = LOWORD(lParam);
        windowData_.clientHeight = HIWORD(lParam);
        if (d3d_ && d3d_->GetDeviceReady())
        {
            if (wParam == SIZE_MINIMIZED)
            {
                windowData_.appPaused = true;
                windowData_.minimized = true;
                windowData_.maximized = false;
            }
            else if (wParam == SIZE_MAXIMIZED)
            {
                windowData_.appPaused = false;
                windowData_.minimized = false;
                windowData_.maximized = true;
                d3d_->OnResize(windowData_.clientWidth, windowData_.clientHeight, *d3d_);
            }
            else if (wParam == SIZE_RESTORED)
            {

                // Restoring from minimized state?
                if (windowData_.minimized)
                {
                    windowData_.appPaused = false;
                    windowData_.minimized = false;
                    d3d_->OnResize(windowData_.clientWidth, windowData_.clientHeight, *d3d_);
                }

                // Restoring from maximized state?
                else if (windowData_.maximized)
                {
                    windowData_.appPaused = false;
                    windowData_.maximized = false;
                    d3d_->OnResize(windowData_.clientWidth, windowData_.clientHeight, *d3d_);
                }
                else if (windowData_.resizing)
                {
                    // If user is dragging the resize bars, we do not resize 
                    // the buffers here because as the user continuously 
                    // drags the resize bars, a stream of WM_SIZE messages are
                    // sent to the window, and it would be pointless (and slow)
                    // to resize for each WM_SIZE message received from dragging
                    // the resize bars.  So instead, we reset after the user is 
                    // done resizing the window and releases the resize bars, which 
                    // sends a WM_EXITSIZEMOVE message.
                }
                else // API call such as SetWindowPos or mSwapChain->SetFullscreenState.
                {
                    d3d_->OnResize(windowData_.clientWidth, windowData_.clientHeight, *d3d_);
                }
            }
        }
        return 0;

        // WM_EXITSIZEMOVE is sent when the user grabs the resize bars.
    case WM_ENTERSIZEMOVE:
        windowData_.appPaused = true;
        windowData_.resizing = true;
        return 0;

        // WM_EXITSIZEMOVE is sent when the user releases the resize bars.
        // Here we reset everything based on the new window dimensions.
    case WM_EXITSIZEMOVE:
        windowData_.appPaused = false;
        windowData_.resizing = false;
        if (d3d_)
            d3d_->OnResize(windowData_.clientWidth, windowData_.clientHeight, *d3d_);
        return 0;

        // WM_DESTROY is sent when the window is being destroyed.
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;

        // The WM_MENUCHAR message is sent when a menu is active and the user presses 
        // a key that does not correspond to any mnemonic or accelerator key. 
    case WM_MENUCHAR:
        // Don't beep when we alt-enter.
        return MAKELRESULT(0, MNC_CLOSE);

        // Catch this message so to prevent the window from becoming too small.
    case WM_GETMINMAXINFO:
        ((MINMAXINFO*)lParam)->ptMinTrackSize.x = 200;
        ((MINMAXINFO*)lParam)->ptMinTrackSize.y = 200;
        return 0;

    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}



bool WinUtil::InitMainWindow(int width, int height, HINSTANCE hInstance, const std::string& appName, WNDPROC mssgHandler, bool centreScrn)
{
    // Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

    assert(mssgHandler);
    windowData_.mainWndCaption = appName;
    windowData_.hAppInst = hInstance;
    windowData_.clientWidth = width;
    windowData_.clientHeight = height;

    WNDCLASS wc;
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = mssgHandler;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = windowData_.hAppInst;
    wc.hIcon = LoadIcon(0, IDI_APPLICATION);
    wc.hCursor = LoadCursor(0, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)GetStockObject(NULL_BRUSH);
    wc.lpszMenuName = 0;
    wc.lpszClassName = "D3DWndClassName";

    if (!RegisterClass(&wc))
    {
        MessageBox(0, "RegisterClass Failed.", 0, 0);
        return false;
    }

    // Compute window rectangle dimensions based on requested client area dimensions.
    RECT R = { 0, 0, windowData_.clientWidth, windowData_.clientHeight };
    AdjustWindowRect(&R, windowData_.winStyle, false);
    int w = R.right - R.left;
    int h = R.bottom - R.top;

    int offsetX = CW_USEDEFAULT, offsetY = CW_USEDEFAULT;
    if (centreScrn) {
        offsetY = (GetSystemMetrics(SM_CYSCREEN) - h) / 2;
        offsetX = (GetSystemMetrics(SM_CXSCREEN) - w) / 2;
    }
    windowData_.hMainWnd = CreateWindow("D3DWndClassName", windowData_.mainWndCaption.c_str(),
        windowData_.winStyle, offsetX, offsetY, w, h, 0, 0, windowData_.hAppInst, 0);
    if (!windowData_.hMainWnd)
    {
        MessageBox(0, "CreateWindow Failed.", 0, 0);
        return false;
    }

    ShowWindow(windowData_.hMainWnd, SW_SHOW);
    UpdateWindow(windowData_.hMainWnd);

    return true;
}


int WinUtil::Run(void(*pUpdate)(float), void(*pRender)(float))
{
    MSG msg = { 0 };
    assert(pUpdate && pRender);

    __int64 countsPerSec;
    QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);
    double secondsPerCount = 1.0 / (double)countsPerSec;

    float deltaTime = 0;
    while (msg.message != WM_QUIT)
    {
        // If there are Window messages then process them.
        if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        // Otherwise, do animation/game stuff.
        else
        {
            if (!windowData_.appPaused)
            {

                if (!windowData_.appPaused)
                    pUpdate(deltaTime);
                pRender(deltaTime);

                static __int64 sTime1 = 0;
                __int64 time2;
                QueryPerformanceCounter((LARGE_INTEGER*)&time2);
                if (sTime1 != 0)
                    deltaTime = (float)((time2 - sTime1) * secondsPerCount);
                sTime1 = time2;
                AddSecToClock(deltaTime);


            }
            else
            {
                Sleep(100);
            }
        }
    }
    return (int)msg.wParam;
}


bool WinUtil::BeginLoop(bool& canUpdateRender)
{
    MSG msg = { 0 };
    canUpdateRender = false;

    // If there are Window messages then process them.
    while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
        if (msg.message == WM_QUIT)
            return false;
    }

    if (!windowData_.appPaused)
    {
        //QueryPerformanceCounter((LARGE_INTEGER*)&sTime1);
        canUpdateRender = true;
    }

    return true;
}


float WinUtil::EndLoop(bool didUpdateRender)
{
    if (!didUpdateRender)
        return 0;
    float deltaTime = 0;
    if (!windowData_.appPaused)
    {
        __int64 countsPerSec;
        QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);
        double secondsPerCount = 1.0 / (double)countsPerSec;
        static __int64 sTime1 = 0;
        __int64 time2;
        QueryPerformanceCounter((LARGE_INTEGER*)&time2);
        if (sTime1 != 0)
            deltaTime = (float)((time2 - sTime1) * secondsPerCount);
        sTime1 = time2;
        AddSecToClock(deltaTime);

    }
    else
    {
        Sleep(100);
    }
    return deltaTime;
}


void WinUtil::ChooseRes(int& width, int& height, int defaults[], int numPairs)
{
    assert(defaults && numPairs > 0);

    //Get screen width/height from system
    int scrnWidth = GetSystemMetrics(SM_CXSCREEN);
    int scrnHeight = GetSystemMetrics(SM_CYSCREEN);

    width = 0;
    height = 0;
    for (unsigned short int i(0); i < numPairs; ++i)
    {
        int newWidth = defaults[i * 2];
        int newHeight = defaults[i * 2 + 1];

        if ((width <= newWidth) && (newWidth <= scrnWidth) && (newHeight <= scrnHeight))
        {
            width = newWidth;
            height = newHeight;
        }
    }
    assert(width > 0);
}

