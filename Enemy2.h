#pragma once

#include "EntityInterface.h"    //Object Parent
#include "Sprite.h"             //Sprite class
#include "StageOneMode.h"       //Mode Owner
#include "Player.h"
#include "FlagSets.h"           //Object state flags

/*
    Basic enemy with the ability to fire at a target (namely the player)
*/

class Enemy2 : public iEntity
{
public:

    Enemy2();
    ~Enemy2()
    {}

    //Parent overrides
    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ResetVariables() override;

    //Set parent mode
    void SetMode(StageOneMode& mode) { ownerMode_ = &mode; }


private:
    //What mode does this sprite belong to
    StageOneMode* ownerMode_ = nullptr;

    //Track when the enemy can shoot
    float shotTimer_ = 0;

    //Flags
    ObjectStateFlags flags1_;

    //Initialise block
    void Init() override;
    //Manage firing projectile
    void FireProjectile();
};