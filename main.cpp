#include <windows.h>
#include <string>
#include <cassert>
#include <iostream>

#include "WindowUtils.h"
#include "Game.h"
#include "AudioMgrFMOD.h"

using namespace std;

//if ALT+ENTER or resize or drag window we might want do
//something like pause the game perhaps, but we definitely
//need to let D3D know what's happened (OnResize_Default).
void OnResize(int screenWidth, int screenHeight, MyD3D& d3d)
{
    d3d.OnResize_Default(screenWidth, screenHeight);
}

//Windows message pump
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    //Game specifics here
    switch (msg)
    {
        //Slower, windows input checking
    case WM_CHAR:
        Game::Get().ProcessKey(static_cast<char>(wParam));
        break;

        //Raw input checking
    case WM_INPUT:
        Game::Get().GetMK().MessageEvent((HRAWINPUT)lParam);
        break;
    }

    //default message handling (resize window, full screen, etc)
    return WinUtil::Get().GetDefaultMssgHandler(hwnd, msg, wParam, lParam);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
    PSTR cmdLine, int showCmd)
{
    //Define window sizes
    int w(1), h(1);
    int defaults[] = { 640,480, 800,600, 1024,768, 1280,1024, 1366,768, 1600,900, 1920,1080 };

    //Create new WinUtil object and initialise window
    new WinUtil();
    WinUtil::Get().ChooseRes(w, h, defaults, 7);
    if (!WinUtil::Get().InitMainWindow(w, h, hInstance, "Project: VALKYRIE", MainWndProc, true))
        assert(false);

    //Initialise D3D
    MyD3D d3d;
    if (!d3d.InitDirect3D(OnResize))
        assert(false);

    //Get winutil d3d handle
    WinUtil::Get().SetD3D(d3d);

    //Initialise file system for audio manager
    File::initialiseSystem();
    new AudioMgrFMOD();
    new Game(d3d);

    //Explicit asset path setup
    d3d.GetCache().SetAssetPath("data/");

    //Handy Handles
    Game& game = Game::Get();
    WinUtil& win = WinUtil::Get();

    //Start Game loop
    bool canUpdateRender;
    float dTime = 0;
    while (WinUtil::Get().BeginLoop(canUpdateRender))
    {
        if (canUpdateRender && dTime > 0)
        {
            //Update Variables etc.
            AudioMgrFMOD::Get().Update();
            game.Update(dTime);
            //Render sprites, text etc
            game.Render(dTime);
        }
        //Get new dTime
        dTime = WinUtil::Get().EndLoop(canUpdateRender);

    }

    //Clean up resources
    game.ReleaseGame();
    delete& Game::Get();
    delete& WinUtil::Get();
    delete& AudioMgrFMOD::Get();

    //True for extra debugging
    d3d.ReleaseD3D(false);

    return 0;
}