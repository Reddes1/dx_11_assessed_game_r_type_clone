#pragma once                    
#include "EntityInterface.h"    //Parent Object

using namespace DirectX;

/*
    Basic Enemy Projectile.
*/
class EnemyProjectile1 : public iEntity
{
public:

    EnemyProjectile1();
    ~EnemyProjectile1()
    { }

    void Update(float dTime) override;

private:

    void Init() override;
};