#include "PowerUp.h"            //Owner
#include "GameConstants.h"      //const values
#include "Tools.h"              //Random method

PowerUp::PowerUp()
    : iEntity(Game::Get().GetD3D())
{
    Init();
}

void PowerUp::Init()
{
    //Handy Handles
    MyD3D& d3d = Game::Get().GetD3D();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Load texture and set frames
    std::vector<RECTF> frames(FD::stageOneEnemyAtlasFrames,
        FD::stageOneEnemyAtlasFrames + sizeof(FD::stageOneEnemyAtlasFrames) / sizeof(FD::stageOneEnemyAtlasFrames[0]));
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "data/sprites/stageone/stageoneatlas.dds",
        "Stage One Atlas 1", false, &frames);

    //Calulate and capture adjustments for screen size
    adj_.scaleAdj = Vector2(width / FC::DEFAULT_RES.x, height / FC::DEFAULT_RES.y);
    adj_.spdAdj = Vector2(width / FC::DEFAULT_RES.x, height / FC::DEFAULT_RES.y);

    //Set object parameters
    spr_.SetTexture(*p);
    spr_.SetOrigin(Vector2((frames[18].right - frames[18].left) * 0.5f, (frames[18].bottom - frames[18].top) * 0.5f));
    spr_.SetScale(Vector2(adj_.scaleAdj.x * GC::PU_SCALE, adj_.scaleAdj.y * GC::PU_SCALE));
    spr_.GetAnimation().Initialise(18, 21, 10, false);
    health_ = GC::PU_STARTING_HP;

    //Set state
    state = STATES::CONTAINER;
}

void PowerUp::Update(float dTime)
{
    //If HP = 0, and flagged that the object just died.
    if (health_ <= 0 && state == STATES::CONTAINER && isActive_)
    {
        //Change state to powerup
        state = STATES::POWER_UP;
        ProcessStateSwitch();
    }
    if (isActive_)
    {
        //Update based on state
        switch (state)
        {
        case(STATES::CONTAINER):
            UpdateContainer(dTime);
            break;
        case(STATES::POWER_UP):
            UpdatePowerUp(dTime);
            break;
        }
    }
    //Update animation
    spr_.GetAnimation().Update(dTime);
}

void PowerUp::ProcessStateSwitch()
{
    switch (state)
    {
    case(STATES::CONTAINER):    //Entered when resetting the object variables
        //Set Frame back to container
        spr_.GetAnimation().SetFrame(spr_.GetAnimation().GetStartFrame());
        //Reset HP
        health_ = GC::PU_STARTING_HP;
        //Reset flags
        isNowOnScreen = false;
        //Get a new velocity
        spr_.SetVelocity(Vector2(GetRandom(-1, -0.5), GetRandom(-1, 1)));
        break;
    case(STATES::POWER_UP):     //Entered when turning from container to power-up
        //Set new velocity
        spr_.SetVelocity(Vector2(-1, 0));
        //Make sure we have no spriteeffects lingering
        spr_.SetSpriteEffect(SprEffects::NONE);
        //Generate a random number from a range, and set power-up type from it
        int num = GetRandNumFromRange(2);
        switch (num)
        {
        case(1):
            powerUpType = POWER_UP_TYPE::SPEED;
            spr_.GetAnimation().SetFrame(19);
            break;
        case(2):
            powerUpType = POWER_UP_TYPE::WEAPON;
            spr_.GetAnimation().SetFrame(20);
            break;
        case(3):
            break;
        }
        break;
    }
}

void PowerUp::Render(float dTime, DirectX::SpriteBatch& batch)
{
    //No special rendering, let the parent handle it
    iEntity::Render(dTime, batch);
}

void PowerUp::ResetVariables()
{
    //Reset state back to container
    state = STATES::CONTAINER;
    //Process how we reset specifically on the object state
    ProcessStateSwitch();
}

void PowerUp::UpdateContainer(float dTime)
{
    //Calculate new position
    Vector2 pos = spr_.GetPosition();
    //Calculate new position
    pos += spr_.GetVelocity() * (GC::PU_SPEED * adj_.spdAdj) * dTime;
    //Validate the new player position
    ValidatePosition(pos);
    //Set new posiiton
    spr_.SetPosition(pos);
}

void PowerUp::UpdatePowerUp(float dTime)
{
    Vector2 pos = spr_.GetPosition();
    
    //Calculate new position
    pos += spr_.GetVelocity() * (GC::PU_SPEED * adj_.spdAdj) * dTime;

    //Set new posiiton
    spr_.SetPosition(pos);

    //Capture screensize
    int width = WinUtil::Get().GetData().clientWidth;
    int height = WinUtil::Get().GetData().clientHeight;

    Vector2 screenSize = spr_.GetScreenSize() * 0.5;
    //Simple off-screen check, go inactive if true
    if (pos.x < 0 - screenSize.x || pos.x > width + screenSize.x || pos.y < 0 - screenSize.y || pos.y > height + screenSize.y)
        isActive_ = false;
}

void PowerUp::ValidatePosition(Vector2& pos)
{

    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;


    RECTF playArea = ownerMode_->GetPlayArea();
    /*
    We want the object come on screen normally, but bounce of the edges once on it,
    so once it is on screen we allow the object to bounce and snap back into the screen from then.
    */
    if (pos.x < playArea.right && pos.y < playArea.bottom)
        isNowOnScreen = true;

    if (isNowOnScreen)
    {
        /*
            Check against the play area, and bounce off of it and reverse the relative velocity, flipping the sprite
            to match the direction.
        */
        if (pos.x < playArea.left)         //Left side
        {
            pos.x = playArea.left;
            Vector2 vel = spr_.GetVelocity();
            vel.x = -vel.x;
            spr_.SetVelocity(vel);
            spr_.SetSpriteEffect(SprEffects::FLIP_HORIZONTAL);
        }
        else if (pos.x > playArea.right)   //Right
        {
            pos.x = playArea.right;
            Vector2 vel = spr_.GetVelocity();
            vel.x = -vel.x;
            spr_.SetVelocity(vel);
            spr_.SetSpriteEffect(SprEffects::FLIP_HORIZONTAL);
        }
        if (pos.y < playArea.top)          //Top
        {
            pos.y = playArea.top;
            Vector2 vel = spr_.GetVelocity();
            vel.y = -vel.y;
            spr_.SetVelocity(vel);
        }
        else if (pos.y > playArea.bottom)  //Bottom
        {
            pos.y = playArea.bottom;
            Vector2 vel = spr_.GetVelocity();
            vel.y = -vel.y;
            spr_.SetVelocity(vel);
        }
    }
}


