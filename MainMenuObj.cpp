#include "MainMenuObj.h"
#include "GameConstants.h"

MainMenuObj::MainMenuObj()
    :iEntity(Game::Get().GetD3D())
{
    Init();
}

MainMenuObj::~MainMenuObj()
{

}

void MainMenuObj::Init()
{
    //Reserve space
    menuSprites_.reserve(4);

    //Handy Handles
    Game& game = Game::Get();
    MyD3D& d3d = game.GetD3D();
    WinUtil& winUtil = WinUtil::Get();

    //Get screen dims
    int width = winUtil.GetData().clientWidth;
    int height = winUtil.GetData().clientHeight;

    //Use the iEntity sprite as a ghost container and set its parameters
    spr_.SetTextureRect(RECTF(0, 0, 1366, 768));
    spr_.SetScale(Vector2(width / spr_.GetTextureRect().x, height / spr_.GetTextureRect().y));
    spr_.SetOrigin(Vector2(spr_.GetTextureRect() * 0.5f));
    spr_.SetPosition(Vector2(width * 0.5f, height * 0.5f));

    //Set up container buttons
    menuSprites_.insert(menuSprites_.begin(), FC::MENU_BUTTON_COUNT, Sprite(game.GetD3D()));

    //Load texture and frames
    std::vector<RECTF> frames(menuFrames, menuFrames + sizeof(menuFrames) / sizeof(menuFrames[0]));
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "data/sprites/mainmenu/menusprites.dds",
        "Main Menu Sprites", false, &frames);

    //Configure the menu
    for (int i(0); i < FC::MENU_BUTTON_COUNT; ++i)
    {
        menuSprites_[i].SetTexture(*p);
        menuSprites_[i].SetOrigin(Vector2(frames[0].right * 0.5f, frames[0].bottom * 0.5f));
        menuSprites_[i].SetScale(spr_.GetScale());
        menuSprites_[i].GetAnimation().Initialise(i * 2, 1 + (i * 2), 0, false);
        menuSprites_[i].SetPosition(Vector2(spr_.GetScreenSize().x * 0.75f, (spr_.GetScreenSize().y * 0.5f) + (menuSprites_[i].GetScreenSize().y * 1.1f) * i));
        menuSprites_[i].GetAnimation().Play(false);
    }

    //Assign flags
    isActive_ = true;
}

void MainMenuObj::Update(float dTime)
{
    //Handy Handle
    Game& game = Game::Get();
    
    HandleGPInputs();

    //Check for collision for each menu item, and then when collision has been detected then check if the player clicked a menu item
    for (unsigned int i(0); i < menuSprites_.size(); ++i)
    {
        //If mouse collision true with menu item
        if (game.GetCollisionManager().Rect2RectCollision(menuSprites_[i].GetCollisionValues(), game.GetMK().GetMouseCollider()))
        {
            //Set the frame
            menuSprites_[i].SetFrame(1 + (i * 2));
            
            //Check if we clicked
            if (i == 0 && game.GetMK().GetMouseButton(MouseAndKeys::ButtonT::LBUTTON))
                game.GetModeManager().SwitchMode("STAGE_ONE");
            else if (i == 1 && game.GetMK().GetMouseButton(MouseAndKeys::ButtonT::LBUTTON))
                game.GetModeManager().SwitchMode("HOW_TO_PLAY");
            else if (i == 2 && game.GetMK().GetMouseButton(MouseAndKeys::ButtonT::LBUTTON))
                PostQuitMessage(0);

        }
        //No mouse collision so set the frame back to normal
        else
            menuSprites_[i].SetFrame(i * 2);
    }
}

void MainMenuObj::Render(float dTime, DirectX::SpriteBatch& batch)
{
    for (auto& m : menuSprites_)
    {
        m.Draw(batch);
    }
}

void MainMenuObj::HandleGPInputs()
{
    //Handy Handle
    Game& game = Game::Get();

    //Check if any of the buttons assigned to menu functions have been pressed
    if(game.GetGP().IsPressed(0, XINPUT_GAMEPAD_A))
        game.GetModeManager().SwitchMode("STAGE_ONE");
    else if (game.GetGP().IsPressed(0, XINPUT_GAMEPAD_BACK))
        PostQuitMessage(0);
    else if (game.GetGP().IsPressed(0, XINPUT_GAMEPAD_Y))
        game.GetModeManager().SwitchMode("HOW_TO_PLAY");
}
