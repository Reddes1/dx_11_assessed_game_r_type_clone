#include "EnemyProjectile1.h"
#include "GameConstants.h"

EnemyProjectile1::EnemyProjectile1()
    :iEntity(Game::Get().GetD3D())
{
    Init();
}

void EnemyProjectile1::Update(float dTime)
{
    if (isActive_)
    {
        //Calculate new position
        Vector2 pos = spr_.GetPosition();
        pos += spr_.GetVelocity() * (GC::E2_PROJ_SPEED * adj_.spdAdj) * dTime;
        spr_.SetPosition(pos);

        int width = WinUtil::Get().GetData().clientWidth;
        int height = WinUtil::Get().GetData().clientHeight;

        //Set the projectile to inactive when offscreen
        if (pos.x < 0 || pos.x > width || pos.y < 0 || pos.y > height)
            isActive_ = false;
    }
}

void EnemyProjectile1::Init()
{
    //Handy Handles
    MyD3D& d3d = Game::Get().GetD3D();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Calulate and capture adjustments for screen size
    adj_.scaleAdj = Vector2(width / FC::DEFAULT_RES.x, height / FC::DEFAULT_RES.y);
    adj_.spdAdj = Vector2(width / FC::DEFAULT_RES.x, height / FC::DEFAULT_RES.y);

    //Load texture and frames
    std::vector<RECTF> frames(FD::projectileAtlas1Frames, FD::projectileAtlas1Frames + 
        sizeof(FD::projectileAtlas1Frames) / sizeof(FD::projectileAtlas1Frames[0]));
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "data/sprites/projectileatlas.dds", "Projectile Atlas 1", false, &frames);

    spr_.SetTexture(*p);
    spr_.SetTextureRect(frames[3]);
    spr_.SetScale(Vector2(adj_.scaleAdj.x * GC::E2_PROJ_SCALE, adj_.scaleAdj.y * GC::E2_PROJ_SCALE));
    spr_.SetOrigin(Vector2((frames[3].right - frames[3].left) * 0.5f, (frames[3].bottom - frames[3].top) * 0.5f));
}
