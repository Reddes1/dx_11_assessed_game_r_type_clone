#include "HighScoreMode.h"
#include "GameConstants.h"      //consts namespaces
#include "Tools.h"           //char checking

HighScoreMode::HighScoreMode()
    :font1_(nullptr)
{
    InitBackground();

    //Load font
    font1_ = new SpriteFont(&Game::Get().GetD3D().GetDevice(), L"data/fonts/algerian.spritefont");

}   

HighScoreMode::~HighScoreMode()
{
    Release();
}

void HighScoreMode::Enter()
{
    //Update Game State and set internal mode to player input
    Game::Get().SetGameState(GameState::HIGH_SCORES);
    state = MODE_STATES::PLAYER_INPUT;

    //Flag input prevention for a short period of time
    modeStates_.stopInput = true;

    //Clear the tempName to prevent echoing from previous enter.
    tempName_.clear();
}

bool HighScoreMode::Exit()
{
    Game::Get().GetScoreManager().SaveDataToFile(); //Uncomment when fixed
    return true;
}

void HighScoreMode::Release()
{
    //clear vectors
    backgrounds_.clear();

    //Release Memory
    delete font1_;
    font1_ = nullptr;
}

void HighScoreMode::Update(float dTime)
{
    if(Game::Get().GetGP().IsPressed(0, XINPUT_GAMEPAD_START))
        Game::Get().GetModeManager().SwitchMode("MAIN_MENU");
}

void HighScoreMode::Render(float dTime, DirectX::SpriteBatch& batch)
{
    for (auto& b : backgrounds_)
    {
        b.Draw(batch);
    }

    RenderManager(batch);
}

void HighScoreMode::ProcessKey(char key)
{
    switch (key)
    {
    case VK_ESCAPE:
        if (state == MODE_STATES::DISPLAY_SCORES)
            Game::Get().GetModeManager().SwitchMode("MAIN_MENU");
        break;
    case VK_BACK:
        if (state == MODE_STATES::PLAYER_INPUT)
            if (!tempName_.empty())
                tempName_.pop_back();
        break;
    case VK_RETURN:
        if (state == MODE_STATES::PLAYER_INPUT)
        {
            AddTableEntry(); //Add the name + entry to the table
            state = MODE_STATES::DISPLAY_SCORES;
        }
        break;
    default:
        if (state == MODE_STATES::PLAYER_INPUT)
            AddChar(key);
        break;
    }
}

void HighScoreMode::InitBackground()
{
    assert(backgrounds_.empty());
    //Handy Handles
    Game& game = Game::Get();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Insert sprite and load texture
    backgrounds_.insert(backgrounds_.begin(), FC::HIGH_SCORE_BG_COUNT, Sprite(game.GetD3D()));
    ID3D11ShaderResourceView* p = Game::Get().GetD3D().GetCache().LoadTexture(&game.GetD3D().GetDevice()
        , "data/backgrounds/backgroundatlas1.dds", "Background Atlas 1", false);

    //Configure sprite
    backgrounds_[0].SetTexture(*p);
    backgrounds_[0].SetTextureRect(FD::backgroundAtlasFrames[2]);
    backgrounds_[0].SetScale(Vector2(width / (FD::backgroundAtlasFrames[0].right - FD::backgroundAtlasFrames[0].left),
        height / (FD::backgroundAtlasFrames[0].bottom - FD::backgroundAtlasFrames[0].top)));
}

void HighScoreMode::RenderManager(DirectX::SpriteBatch& batch)
{
    //Render different screen text based on mode state
    switch (state)
    {
        case(MODE_STATES::DISPLAY_SCORES):
            RenderScores(batch);
            break;
        case(MODE_STATES::PLAYER_INPUT):
            RenderInput(batch);
            break;
    }
}

void HighScoreMode::RenderInput(DirectX::SpriteBatch& batch)
{
    //Handy Handles
    Game& game = Game::Get();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //String container
    std::string message = "Please Enter Your Name (Max 5 Chars):";

    Vector2 pos{ width * 0.5f, height * 0.5f };
    RECT textDim = font1_->MeasureDrawBounds(message.c_str(), Vector2(0, 0));
    //Draw enter prompt
    font1_->DrawString(&batch, message.c_str(), pos, Colours::White, 0, Vector2((float)textDim.right * 0.5f, 1));

    //Draw player name
    pos = Vector2(width * 0.5f, height * 0.58f);
    textDim = font1_->MeasureDrawBounds(tempName_.c_str(), Vector2(0, 0));
    font1_->DrawString(&batch, tempName_.c_str(), pos, Colours::White, 0, Vector2((float)textDim.right * 0.5f, 1));
}

void HighScoreMode::RenderScores(DirectX::SpriteBatch& batch)
{
    //Handy Handles
    Game& game = Game::Get();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //String containers
    std::string message = "Top Ten Highscore";
    std::string name;
    std::string score;

    //Starting position for the table
    Vector2 pos{ width * 0.5f, height * 0.1f };
    RECT textDim = font1_->MeasureDrawBounds(message.c_str(), Vector2(0,0));
    
    //Set new pos value for use in the loop below
    pos = { width * 0.5f, height * 0.2f };

    font1_->DrawString(&batch, message.c_str(), pos, Colours::White, 0, Vector2((float)textDim.right * 0.5f, 1));

    //Position and display 10 scores
    for (int i(0); i < 10; ++i)
    {
        //Clear the previous string data
        message.clear();

        //Get the player name and score, then convert score to string
        name = game.GetScoreManager().GetPlayerNameFromTable(i);
        int tempScore = game.GetScoreManager().GetPlayerScoreFromTable(i);
        score = std::to_string(tempScore);

        //Append together the table entry for viewing
        message.append(name);
        message.append("   ");
        message.append(score);

        //Measure the size of the string
        textDim = font1_->MeasureDrawBounds(message.c_str(), Vector2(0, 0));

        //Add the height of the string (with additional spacing offset) to the position to move the list down each entry
        pos.y += textDim.bottom * 1.2f;

        //Draw
        font1_->DrawString(&batch, message.c_str(), pos, Colours::White, 0, Vector2((float)textDim.right * 0.5f, 1));
    }

    //Render mode navigation message
    pos = Vector2(width * 0.5f, height * 0.8f);
    message = "Press Start/Escape to Continue!";
    textDim = font1_->MeasureDrawBounds(message.c_str(), Vector2(0, 0));
    //Draw
    font1_->DrawString(&batch, message.c_str(), pos, Colours::White, 0, Vector2((float)textDim.right * 0.5f, 1));

}

void HighScoreMode::AddTableEntry()
{
    //Handy Handle
    Game& game = Game::Get();

    //Assign name to the current player
    game.GetScoreManager().SetActivePlayerName(tempName_);

    //Add our new entry to the table
    game.GetScoreManager().AddActivePlayerToTable();
}

void HighScoreMode::AddChar(char character)
{
    //Validate that the character is valid 
    if (ValidateCharAz(character))
        tempName_ += character;

    //If the string is now bigger than 5, pop the back off to keep it in size
    if (tempName_.size() > MC::NAME_MAX_SIZE)
        tempName_.pop_back();
}

