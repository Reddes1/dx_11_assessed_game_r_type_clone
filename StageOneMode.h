#pragma once
#include <vector>

#include "ModeInterface.h"   //For mode interface
#include "Sprite.h"          //For making sprite vector arrays
#include "EntityInterface.h" //For making entity vector arrays

//Used to determine stage states
enum class STAGE_STATES
{
    STATE_1, STATE_2, STATE_3, STATE_4, STATE_5, STATE_6, STATE_7, STATE_8
};
/*
    First Stage for the game.
*/
class StageOneMode : public iMode
{
public:
    //Mode handle
    inline static const std::string MODE_NAME = "STAGE_ONE";

    StageOneMode();
    ~StageOneMode();

    //Parent overrides
    void Enter() override;
    bool Exit() override;
    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ProcessKey(char key) override;
    std::string GetModeName() const override { return MODE_NAME; }

    //Find the first matching object of given type and active state
    iEntity* FindFirstObject(const std::type_info& type, bool active);

    //Find the first matching projectile of given type and active state
    iEntity* FindFirstProjectile(const std::type_info& type, bool active);

    //Return the play area for the level
    RECTF& GetPlayArea();

private:

    //Containers for our different object types (backgrounds, bodies, projectiles etc)
    std::vector<Sprite> backgroundScene_;
    std::vector<iEntity*> gameObjects_;
    std::vector<iEntity*> projObjects_;

    //Spritefont for drawing the score
    DirectX::SpriteFont* font1_ = nullptr;

    //Manage current stage state
    STAGE_STATES currentState_;

    RECTF playArea_;
    
    //Stage timers
    struct Timers
    {
        float e1SpawnElapsed_ = -2;
        float e2SpawnElapsed_ = -2;
        float puSpawnElapsed_ = 0;
        float stageElapsed_ = 0;
        float stageSpawnerElapsed_ = 0;
    }; 
    Timers timers_;

    //Mode initialisers
    void Init();
    //Allocates new objects for each container.
    void InitObjectContainers();
    //Loads & set texture, inserts background layers into container. 
    void InitBackground();
    //Configures the background for the gamespace
    void ConfigureBackground();
    //Resets the game values and player position to starting state
    void ConfigureGameState();

    //Process the stage state and spawn enemies based on it
    void UpdateStageState();
    //All collision checks run through here
    void ManageCollisions();
    //Enemy>Player interactions
    void EnemyCollisions(iEntity* p);
    //Process an upgrade when colliding with power-up
    void ProcessUpgradeEffect(iEntity* p, iEntity* pUp);
    //Player>Others interactions
    void PlayerCollisions(iEntity* p);
    //Update the background scene
    void UpdateBackground(float dTime);
    //Update any of the stage timers here
    void UpdateStageTimers(float dTime);
    //Render screen text like score
    void RenderText(DirectX::SpriteBatch& batch);
    //Update any inputs from gamepad
    void UpdateGPInputs();

    //Spawns enemy of a given type with modifiable parameters
    void SpawnEnemy(const std::type_info& enemyType, float yPos, float spawnCount);
    //Make all objects from object arrays inactive
    void MakeAllInactive();

    //Add a object into select container
    void Add(std::vector<iEntity*>& container, iEntity* object);
    //Delete a object in the container 
    void Remove(std::vector<iEntity*>& container, iEntity* object);
    //More dynamic FindFirst function.
    iEntity* FindFirst(std::vector<iEntity*>& container, const std::type_info& type, bool active);

    //Player has died, set flags, process data etc here before exiting mode
    void ProcessGameOver();
    //Player has paused, set flags, process data etc here before exiting mode
    void ProcessPause();
    //Clean up
    void Release();
};